from hwy_hw import *
from hwy_setting import *
from hwy_pulse import *

class Boss(DdsManager):
    def __init__(self, hwy_base_addr):
        super(Boss,self).__init__(hwy_base_addr)
        self.dds = []
        self.seq = []
        self.m_has_new_pulse = [0 for i in range(0,HWY_DDS_CHAN)]
        self.m_synced = True
        self.m_in_subloop = False
        self.m_new_pulse = None
        self.init_sequences()
    
    def init_sequences(self):
        for k in range(0,HWY_DDS_CHAN):
            # channel masks auto-generated
            self.dds[k] = LoopSequence(self, True, 0x00000001 << k)
            # initialise pointers
            self.seq[k] = self.dds[k]

            self.m_has_new_pulse[k] = False

    def append(self, p, chan_mask, check_sync):
        self.append_ns(p, chan_mask, check_sync)
        self.sync()
        
    def append_all(self, p):
        assert(self.m_synced)
        for i in self.seq:
            i.append(p)

    def append_ns(self, p, chan_mask, check_sync):
        assert(chan_mask is not 0)

        temp_mask = 0x00000001

        if self.m_synced:   # first Pulse being added to this timeslot
            self.m_synced = False
            # check that there's no funny business
            assert(self.m_new_pulse == None)

            # Save the new Pulse
            self.m_new_pulse = p
            for k in range(0, HWY_DDS_CHAN):
                if temp_mask & chan_mask:
                    # Add the Pulse to the channel Sequence
                    self.seq[k].append(p)
                    # Record that a pulse has been added to this timeslot already
                    self.m_has_new_pulse[k] = True
                temp_mask = temp_mask << 1
        else: # not the first Pulse being added to this timeslot
            if check_sync:
                # Check that this Pulse's total time matches the time of the Pulses
                # already added to this timeslot. Pointers are compared, so could be two NULLs
                assert(p.get_extra_delay_TimeSetting() == self.m_new_pulse.get_extra_delay_TimeSetting())
                # Check that this Pulse's to-be-altered TimeSetting is the same
                # one that could be altered by other Pulses
                assert(p.get_TimeSetting() == self.m_new_pulse.get_TimeSetting())

            for k in range(0,HWY_DDS_CHAN):
                if temp_mask & chan_mask:
                    # Make sure there is no new Pulse already in this timeslot on this channel
                    assert(not self.m_has_new_pulse[k])
                    # Add the POulse to the channel Sequence
                    self.seq[k].append(p)
                    # Record that a pulse has been added to this timeslot already
                    self.m_has_new_pulse[k] = True
                temp_mask = temp_mask << 1

    def sync(self):
        assert(not self.m_synced)

        delay = self.m_new_pulse.get_TimeSetting()
        extra_delay = self.m_new_pulse.get_extra_delay_TimeSetting()

        # If the current user-added Pulse's TimeSetting is null, the Pulse added by the user
        # to this time window can't be swept.
        delay_present = (delay is not None)
        extra_delay_present = (extra_delay is not None)
        if extra_delay_present:
            wp_extra = WaitPulse(extra_delay, self, False)
        if delay_present:
            wp = WaitPulse(delay, self, False)
        # Add dummy Pulses
        for k in range(0, HWY_DDS_CHAN):
            if not self.m_has_new_pulse[k]:
                if delay_present:
                    self.seq[k].append(wp) # Add dummy Pulse linked to same TimeSetting as other pulses
                if extra_delay_present:
                    # Add dummy Pulse to fill in the time
                    self.seq[k].append(wp_extra)
            else:
                # clear the status, in preparation for next time window
                self.m_has_new_pulse[k] = False

        # clear the new Pulse pointer, in preparation for next time window
        self.m_new_pulse = None
        self.m_synced = True

    def start_subloop(self, loops):
        assert(self.m_synced)
        assert(not self.m_in_subloop)

        for k in range(0, HWY_DDS_CHAN):
            sl = SubLoop(loops, self, 0x00000001 <<k) # temporary pointer
            self.dds[k].append(sl) # add SubLoop to Pulses
            self.seq[k] = sl # change seq pointers, so that append_ns() and
                             # sync() put Pulses into the SubLoop
            self.m_in_subloop = True
            # because sl is managed by Boss, no deletion is needed

    def dyn_update(self, s):
        assert(self.m_in_subloop)

        chan_mask = s.get_chan_mask()
        s.register_for_fifo()
        for k in range(0,HWY_DDS_CHAN):
            if (0x00000001<<k) & chan_mask:
                # following is ok, because seq[k] always points to a SubLoop if m_in_subloop is true
                q = self.seq[k]
                q.increment_pops_per_loop(s)


    def end_subloop(self):
        assert(self.m_in_subloop)

        for k in range(0, HWY_DDS_CHAN):
            # change seq pointers, so that things get appended to main DDS
            # Sequences instead of SubLoops from now on
            self.seq[k] = self.dds[k]
        self.m_in_subloop = False

    def send_all(self):
        for i in self.dds:
            self.send(i)
        self.send_settings()

    def clear_all(self):
        self.clear_all()
        self.init_sequences()
        
