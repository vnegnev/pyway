from hwy_hw0 import *

class Setting(DdsBramObject,object):
    #def __init__(self, addr, ddsman, manage, chan_mask):
    #    super(Setting,self).__init__(ddsman,chan_mask)
    #    assert(self.check_addr(addr))
    #    self.m_uses_fifo = false
    #    self.m_addr = addr
    #    if manage:
    #        ddsman.append_setting(self)
    #def __init__(self, ddsman, manage, chan_mask):
    #    super(Setting,self).__init__(ddsman,chan_mask)
    #    if manage:
    #        ddsman.append_setting(self)
    def __init__(self, *args, **kwds):
        if len(args) == 4 or len(args) == 2:
            addr = args[0]
            ddsman = args[1]
            if len(args) == 4:
                manage = args[2]
                chan_mask = args[3]
            else:
                manage = True
                chan_mask = 0
            super(Setting,self).__init__(ddsman,chan_mask)
            self.m_uses_fifo = False
            assert(self.check_addr(addr))
            self.m_addr = addr
            if manage:
                ddsman.append_setting(self)
        elif len(args) == 3 or len(args) == 1:
            ddsman = args[0]
            manage = args[1]
            if len(args) == 3:
                chan_mask = args[2]
            else:
                chan_mask = 0
            super(Setting,self).__init__(ddsman,chan_mask)
            if manage:
                ddsman.append_setting(self)

    def check_data(self,data):
        return ( 0 <= data and data <= 0x3ffff)
    def set_data(self,data):
        ok = self.check_data(data)  # check_data() is pulled from children
        assert(ok),data
        if not ok:
            return -1
        self.m_data = data
        self.m_sent = False
        return 0

    def get_data(self):
        return self.m_data

    def size(self):
        return 1
    
    def set_chan_mask(self,chan_mask):
        if chan_mask != self.m_chan_mask:
            self.m_chan_mask = chan_mask # all 32 bits are used, so any mask is valid
            self.m_sent = False          # set flag indicating that the Setting needs to be sent again

    def send_obj(self):
        ok = not self.is_sent()
        assert(ok)
        if not ok:
            return -1
        if self.m_addr == -1:
            if X86_EMULATION:
                print "# WARNING: SETTING ADDRESS UNINITIALISED! ARE YOU USING ALL THE SETTINGS YOU CREATED?\n"
            return -1
        else:
            return self.send_setting(False) # implemented by children

    def register_for_fifo(self):
        self.m_uses_fifo = True

    def send_to_fifo(self):
        ok = (not self.is_sent()) and self.m_uses_fifo
        assert(ok)
        if not ok:
            return -1
        return self.send_setting(True) # implemented by children

    def HWY_write_Setting(self, bram, addr, data, to_fifo):
        print "DEBUGINFO(setting line 91)bram, addr, data", bram, addr, data
        if addr == -1:
            raw_input()
        self.m_ddsman.send_pkt( HWY_odec_op_gen( bram, addr, data ) )




class SpiFreqSetting(Setting):    
    def __init__(self, *args, **kwds):
        if len(args) == 5:
            addr = args[0]
            data = args[1]
            ddsman = args[2]
            manage = args[3]
            chan_mask = args[4]
            super(SpiFreqSetting,self).__init__(ddsman, manage, chan_mask)
            self.set_addr(addr)
            self.set_data(data)
        elif len(args) == 4:
            data = args[0]
            ddsman = args[1]
            manage = args[2]
            chan_mask = args[3]
            super(SpiFreqSetting,self).__init__(ddsman, manage, chan_mask)
            self.set_data(data)
        elif len(args) == 3:
            addr = args[0]
            data = args[1]
            ddsman = args[2]
            manage = True
            chan_mask = 0 
            super(SpiFreqSetting,self).__init__(ddsman, manage, chan_mask)
            self.set_addr(addr)
            self.set_data(data)
        elif len(args) == 2:
            data = args[0]
            ddsman = args[1]
            manage = True
            chan_mask = 0
            super(SpiFreqSetting,self).__init__(ddsman, manage, chan_mask)
            self.set_data(data)


            
    #def __init__(self, ddr, data, ddsman,  manage,  chan_mask):
    #    super(SpiFreqSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_addr(addr)
    #    self.set_data(data)

    #def __init__(self,data, ddsman, manage, chan_mask):
    #    super(SpiFreqSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_data(data)

    def check_addr(self,addr):
        return 0 <= addr and addr < 511
    
    def check_data(self,data):
        return data < (500 * ((1<<31)/500)); # data < 500 MHz

    def auto_assign_addr(self):
        self.set_addr(self.m_ddsman.allocate_free_addr('spi_br', 2, self.m_chan_mask))

    def size(self):
        return 2

    def send_setting(self, to_fifo=False):
        # 0x0 << 16 and 0x1 << 16: specify the bit alignment of the SPI transfer (see mdds.v)
        print "DEBUGINFO (setting line 157)", self.m_addr, self.m_data
        self.HWY_write_Setting(HWY_MDDS_S_BRAM, self.m_addr, (0x0 << 16) | (self.m_data & 0xffff), to_fifo);
        self.HWY_write_Setting(HWY_MDDS_S_BRAM, self.m_addr+1, (0x1 << 16) | (self.m_data >> 16), to_fifo);
        self.m_sent = True;
        return self.m_addr+2;


class SpiCfrSetting(Setting):
    def __init__(self,*args,**kwds):
        if len(args) == 5 or len(args) == 3:
            addr = args[0]
            data = args[1]
            ddsman = args[2]
            if len(args) == 5:
                manage = args[3]
                chan_mask = args[4]
            else:
                manage = True
                chan_mask = 0
            super(SpiCfrSetting,self).__init__(ddsman, manage, chan_mask)
            self.set_addr(addr)
            self.set_data(data)
        elif len(args) == 4 or len(args) == 2:
             data = args[0]
             ddsman = args[1]
             if len(args) == 4:
                 manage = args[2]
                 chan_mask = args[3]
             else:
                 manage = True
                 chan_mask = 0
             super(SpiCfrSetting,self).__init__(ddsman, manage, chan_mask)
             self.set_data(data)

    #def __init__(self, addr, data, ddsman, manage, chan_mask):
    #    super(SpiCfrSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_addr(addr)
    #    self.set_data(data)

    #def __init__(self, data, ddsman,  manage,  chan_mask):
    #    super(SpiCfrSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_data(data)

    def check_addr(self, addr):
        return (0 <= addr and addr < 511); # takes two BRAM addresses, so can't write to the top one

    def check_data(self, data):
        return True; # all data is potentially valid - can put something more advanced here later

    def auto_assign_addr(self):
        self.set_addr(self.m_ddsman.allocate_free_addr('spi_br', 2, m_chan_mask))

    def size(self):
        return 2

    def send_setting(self, to_fifo=False):
        self.HWY_write_Setting(HWY_MDDS_S_BRAM, self.m_addr, (0x2 << 16) | (self.m_data & 0xffff), to_fifo);
        self.HWY_write_Setting(HWY_MDDS_S_BRAM, self.m_addr+1, (0x3 << 16) | (self.m_data >> 16), to_fifo);
        self.m_sent = True;
        return self.m_addr+2;


class SpiPhaseSetting(Setting):
    def __init__(self, *args, **kwds):
        if len(args) == 5 or len(args) == 3:
            addr = args[0]
            data = args[1]
            ddsman = args[2]
            if len(args) == 5:
                manage = args[3]
                chan_mask = args[4]
            else:
                manage = True
                chan_mask = 0
            super(SpiPhaseSetting,self).__init__(ddsman, manage, chan_mask)
            self.set_addr(addr)
            self.set_data(data)
        elif len(args) == 4 or len(args) == 2:
             data = args[0]
             ddsman = args[1]
             if len(args) == 4:
                 manage = args[2]
                 chan_mask = args[3]
             else:
                 manage = True
                 chan_mask = 0
             super(SpiPhaseSetting,self).__init__(ddsman, manage, chan_mask)
             self.set_data(data)

    #def __init__(self, addr,  data, ddsman,  manage,  chan_mask):
    #    super(SpiPhaseSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_addr(addr)
    #    self.set_data(data)

    #def __init__(self, data, ddsman, manage, chan_mask):
    #    super(SpiPhaseSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_data(data)

    def check_addr(self, addr):
        return (0 <= addr and addr < 512)

    def check_data(self, data):
        return (data >= 0 and data <= 0xffff); #0 - 2pi

    def auto_assign_addr(self):
        assert(m_chan_mask); # make sure a Pulse has actually propagated the channel mask down
        self.set_addr(self.m_ddsman.allocate_free_addr('spi_br', 1, self.m_chan_mask));

    def send_setting(self, to_fifo=False):
        self.HWY_write_Setting(HWY_MDDS_S_BRAM, self.m_addr, (0x2 << 16) | self.m_data, to_fifo);
        self.m_sent = True;
        return self.m_addr+1;
        


class SpiAmpSetting(Setting):
    def __init__(self, *args, **kwds):
        if len(args) == 5 or len(args) == 3:
            addr = args[0]
            data = args[1]
            ddsman = args[2]
            if len(args) == 5:
                manage = args[3]
                chan_mask = args[4]
            else:
                manage = True
                chan_mask = 0
            super(SpiAmpSetting,self).__init__(ddsman, manage, chan_mask)
            self.set_addr(addr)
            self.set_data(data)
        elif len(args) == 4 or len(args) == 2:
             data = args[0]
             ddsman = args[1]
             if len(args) == 4:
                 manage = args[2]
                 chan_mask = args[3]
             else:
                 manage = True
                 chan_mask = 0
             super(SpiAmpSetting,self).__init__(ddsman, manage, chan_mask)
             self.set_data(data)
    #def __init__(self,addr, data, ddsman,  manage, chan_mask):
    #    super(SpiAmpSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_addr(addr)
    #    self.set_data(data)

    #def __init__(self, data, ddsman,  manage, chan_mask):
    #    super(SpiAmpSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_data(data)
    
    def check_addr(self, addr):
        return (0 <= addr and addr < 512) 

    def check_data(self, data):
        return (data >= 0 and data <= 0x3fff ); #0 - full-scale, 14-bit

    def auto_assign_addr(self):
        self.set_addr(self.m_ddsman.allocate_free_addr('spi_br', 1, self.m_chan_mask))

    def send_setting(self, to_fifo=False):
        print "DEBUGINFO (SETTING 319)", self.m_addr
        self.HWY_write_Setting(HWY_MDDS_S_BRAM, self.m_addr, (0x3 << 16) | self.m_data, to_fifo);
        self.m_sent = True;
        return self.m_addr+1;

class vgaSetting(Setting):
    def __init__(self, *args, **kwds):
        if len(args) == 5 or len(args) == 3:
            addr = args[0]
            data = args[1]
            ddsman = args[2]
            if len(args) == 5:
                manage = args[3]
                chan_mask = args[4]
            else:
                manage = True
                chan_mask = 0
            super(vgaSetting,self).__init__(ddsman, manage, chan_mask)
            self.set_addr(addr)
            self.set_data(data)
        elif len(args) == 4 or len(args) == 2:
             data = args[0]
             ddsman = args[1]
             if len(args) == 4:
                 manage = args[2]
                 chan_mask = args[3]
             else:
                 manage = True
                 chan_mask = 0
             super(vgaSetting,self).__init__(ddsman, manage, chan_mask)
             self.set_data(data)
    #def __init__(self,addr, data, ddsman,  manage, chan_mask):
    #    super(VgaSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_addr(addr)
    #    self.set_data(data)

    #def __init__(self, data, ddsman,  manage, chan_mask):
    #    super(VgaAmpSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.set_data(data)

    def auto_assign_addr(self):
        self.set_addr(self.m_ddsman.allocate_free_addr('vga_br', 2, self.m_chan_mask))

    def size(self):
        return 2

    def send_setting(self, to_fifo=False):
        self.HWY_write_Setting(HWY_MVGA_BRAM, self.m_addr, 0x3fff, to_fifo);
        self.HWY_write_Setting(HWY_MVGA_BRAM, self.m_addr+1, m_data, to_fifo);
        self.m_sent = true;
        return self.m_addr+2;

class VgaLutSetting(Setting):
    def __init__(self, vga_lut, ddsman, manage=True, chan_mask=0):
        super(VgaLutSetting).__init__(ddsman, manage, chan_mask)
        self.m_vga_lut = vga_lut
        assert(self.check_vga_lut())
        self.m_addr = HWY_ADDR_UNUSED;
        self.set_chan_mask(chan_mask);

    
    def check_vga_lut(self):
        for k in range(0, self.m_vga_lut.size()):
            if self.m_vga_lut[k] > 0x3ffff:
               return False
        return True

    def auto_assign_addr(self):
        # VgaLutSetting address is always 0, so this should never get called
        assert(0)

    def size(self):
        return self.m_vga_lut.size()

    def send_setting(self, to_fifo=False):
        i = 0
        for lut_el in self.m_vga_lut:
            self.HWY_write_Setting(HWY_MVGA_L_BRAM, i, lut_el, to_fifo)
            i = i+1
        self.m_sent = True
        return self.m_vga_lut.size()



class VgaLutSubsetSetting(Setting):
    def __init__(self, vga_lut_addresses, vga_lut, ddsman, manage=True,  chan_mask = 0):
        super(VgaLutSubsetSetting, self).__init__(ddsman, manage, chan_mask)
        self.m_vga_lut_addresses = vga_lut_addresses
        self.m_vga_lut = vga_lut
        assert(self.check_vga_lut())
        self.m_addr = HWY_ADDR_UNUSED
        self.set_chan_mask(chan_mask)

    def check_vga_lut(self):
        #if self.m_vga_lut.size() != self.m_vga_lut_addresses.size():
        if len(self.m_vga_lut) != len(self.m_vga_lut_addresses):
            return False   #make sure LUT and addresses are the same size
        if len(self.m_vga_lut) > 1024:
            return False   # make sure LUT is not too big
        for i in self.m_vga_lut:
            if i > 0x3ffff:
                return False  #check the LUT is not too big
        for i in self.m_vga_lut_addresses:
            if i > 1024:
                return False
        return True

    def auto_assign_addr(self):
        assert(0)  # This is handled by the vector of addresses, which is set on construction
    
    def size(self):
        return self.m_vga_lut.size()

    def send_setting(self, to_fifo=False):
        for k in range(0, len(self.m_vga_lut)):
            self.HWY_write_Setting(HWY_MVGA_L_BRAM, self.m_vga_lut_addresses[k], self.m_vga_lut[k], to_fifo)
        self.m_sent = True
        return len(self.m_vga_lut)





class TimeSetting(Setting):
    def __init__(self, *args, **kwds):
        self.m_wf_time = False
        if len(args) == 6 or len(args) == 4:
            addr = args[0]
            time = args[1]
            long_time = args[2]
            ddsman = args[3]
            if len(args) == 6:
                manage = args[4]
                chan_mask = args[5]
            else:
                manage = True
                chan_mask = 0
            super(TimeSetting,self).__init__(ddsman, manage, chan_mask)
            self.m_long_time = long_time
            self.set_addr(addr)
            self.set_time(time)
        elif len(args) == 5 or len(args) == 3:
            time = args[0]
            long_time = args[1]
            ddsman = args[2]
            if len(args) == 5:
                manage = args[3]
                chan_mask = args[4]
            else:
                manage = True
                chan_mask = 0
            super(TimeSetting,self).__init__(ddsman,manage, chan_mask)
            self.m_long_time = long_time
            self.set_time(time)

    #def __init__(self, addr, time,  long_time, ddsman,  manage,  chan_mask):
    #    super(TimeSetting,self).__init__(ddsman, manage, chan_mask)
    #    self.m_long_time = long_time
    #    self.set_addr(addr)
    #    self.set_data(data)

    #def __init__(self, time, long_time, ddsman,  manage, chan_mask):
    #    super(TimeSetting,self).__init__(ddsman,manage, chan_mask)
    #    self.m_long_time = long_time
    #    self.set_data(data)

    def check_time(self, time):
         return time <= 0xfffffffff; # 36-bit time word
    
    def is_long_time(self):
        return self.m_long_time

    def set_wf_time(self):
        assert(not self.m_long_time)
        assert(self.m_addr < HWY_START_TMR_ADDR)
        self.m_wf_time = True

    def get_data(self):
        assert(0)

    def set_data(self,data):
        assert(0)

    def set_time(self, time):
        ok = self.check_time(time)
        need_long_time = time > HWY_TIMER_MAX_SHORT_DELAY
        if ((not self.m_long_time) and need_long_time):
            ok = False
        if (self.m_long_time and self.m_wf_time):
            ok = False
        self.m_time = time
        self.m_sent = False

        assert(ok)
        if not ok:
            return -1
        return 0
     
    def get_time(self):
        return self.m_time

    def auto_assign_addr(self):
        self.set_addr(self.m_ddsman.allocate_free_addr('tmr_br', (1+self.is_long_time()), self.m_chan_mask))

    def size(self):
        return 1 + self.m_long_time

    def send_setting(self, to_fifo):
        self.HWY_write_Setting(HWY_MTIM_BRAM, self.m_addr, int(self.m_time) & 0x3ffff, to_fifo);
        self.m_sent = True
        if self.m_long_time:
            self.HWY_write_Setting(HWY_MTIM_BRAM, self.m_addr+1, int(self.m_time) >> 18, to_fifo)
            return self.m_addr+2
        else:
            return self.m_addr+1
