from hwy import *
class DdsBramObject(object):
    def __init__(self, ddsman, chan_mask=0, chan_mask_mutable=True):
        self.m_ddsman = ddsman
        self.m_chan_mask = chan_mask
        self.m_addr = -1
        #self.m_addr = 0xfffe
        self.m_sent = False
        self.m_chan_mask_mutable = chan_mask_mutable
        self.m_settings = []

        if chan_mask:
            self.m_chan_mask_mutable = False

    def has_addr(self):
        return self.m_addr != (-1)
        #return self.m_addr != 0xfffe

    def check_addr(self, addr):
        return (0 <= addr and addr < 512)

    def is_sent(self):
        return self.m_sent

    def send_obja(self,addr):
        if self.has_addr(): #Ensure that, if an address has been assigned
                         # already, the object has previously been sent
                         # out.
            assert(self.is_sent())
            self.m_sent = False
   
        self.m_addr = addr
       #unsigned r = send_obj();
       #m_addr = static_cast<u16>(-1);
       #return r;
        return self.send_obj()

    def send_obj(self):  # declared as virtual
        pass

    def get_addr(self):
        return self.m_addr

    def set_addr(self,addr):
        ok = self.check_addr(addr) and not self.has_addr()  #check_addr() is pulled from children
        assert(ok)
        if(not ok):
            return -1
        self.m_addr = addr;
    #    m_sent = false; // need to resend the Setting
        return 0;
    
    def get_chan_mask(self):
        return self.m_chan_mask

    def chan_mask_mutable(self):
        return self.m_chan_mask_mutable

    def add_to_chan_mask(self,chan_mask):
        # higher-level Pulse (or whatever) needs to check this out itself
        assert(self.m_chan_mask_mutable);
        
        if (self.m_chan_mask != chan_mask):
            self.m_sent = False;
            self.m_chan_mask |= chan_mask;



def HWY_idec_op_gen(op, param, payload, payload_wid):
    assert(payload < (1 << payload_wid));
    assert(param < (1 << (14-payload_wid)));
    #Note: param here is shifted depending on payload width - this
    # is inconsistent with the idecoder.v definition.
    return (op << 14) | (param << payload_wid ) | payload;
   
def HWY_odec_op_gen( bram,  addr,  field):
    return (bram << 28) | (addr << 18) | field;
    
def HWY_odec_ctrl_gen(chan_mask, idec_rst, trig, ctrl_word, update_ctrl_word=True):
    assert (chan_mask > 0 and chan_mask < 16)
    return (1 << 15) | (update_ctrl_word << 14) | (idec_rst << 13) | (trig << 12) | (chan_mask << 8) | (ctrl_word)
    
   #void dds::HWY_set_chan_mask(u8 chan_mask){}
    
def freq_to_ftw(freq):
    return (freq*4294967296.0/1000)
    
def amp_to_atw(amp):
    return (amp*163.83) # 16383 corresponds to max amp for input of 100
    
def phase_to_ptw(phase):
    return ((phase*65536.0/360) % 65536) 
    
def time_to_ticks(microsec):
    return (microsec*1000.0/8 - 1)# TODO: avoid floor error!

