from hwy_dds import *

def hwy_dds_selftest():
    bpm = HwyManager(0)  # initialise it
    f1 = bpm.freq(300)
    a1 = bpm.amp(50)
    ph1 = bpm.ph(0)

    f2 = bpm.freq(100)
    a2 = bpm.amp(50)
    ph2 = bpm.ph(0)

    t1 = bpm.time(100)
    t2 = bpm.time(200)
    t3 = bpm.time(300)
    t4 = bpm.time(200)

    pulses = []
    p1 = bpm.pulse(t1,t2,f1,f1, a1, a1, ph1, ph1, "397 Pi")
    p2 = bpm.pulse(t3, t4, f1, f1, a1, a1, ph1, ph1, "729 Pi/2")

    # How the pulse sequence is defined
    pulses.append(p1)
    pulses.append(p2)

    bp_397 = bpm.seq(pulses, 0x00000001)

    # This is where the magic happens
    bp_397.send()
    return 0

