from hwy_hw0 import *


class Pulse(DdsBramObject,object):
    def __init__(self, ddsman, is_func, manage, chan_mask):
        super(Pulse, self).__init__(ddsman, chan_mask)
        self.m_delay = None
        self.m_extra_delay = None
        self.m_is_func = is_func
        if manage:
            ddsman.append_pulse(self)   # always managed by DdsManager

    def get_TimeSetting(self):
        return self.m_delay

    def get_extra_delay_TimeSetting(self):
        return self.m_extra_delay

    def check_mask_overlap(self):
        for i in self.m_settings:
            assert((self.m_chan_mask & i.get_chan_mask()) == self.m_chan_mask)

    def pass_mask_down(self, *args, **kwds):
        if len(args) == 1:
            chan_mask = args[0]
            self.add_to_chan_mask(chan_mask)
            self.pass_mask_down()
        elif len(args) == 0:
            for i in self.m_settings:
                #print type(i)
                if i.chan_mask_mutable():
                    i.add_to_chan_mask(self.m_chan_mask)


    #def pass_mask_down(self, chan_mask):
   #     self.add_to_chan_mask(chan_mask)
    #    self.pass_mask_down()

    def is_func(self):
        return self.m_is_func

    def needs_pre_update(self):
        return self.m_needs_pre_update

    def check_addr(self,addr):
        return (0 <= addr and addr < 1024)

    #def pass_mask_down(self):
    #    for i in self.m_settings:
    #        if i.chan_mask_mutable():
    #            i.add_to_chan_mask(self.m_chan_mask)

    def set_addr_of_children(self):
        for i in self.m_settings:
            if not i.has_addr():
                i.auto_assign_addr()

    def HWY_write_Instr(self, addr, op, param, payload, payload_wid):
        self.m_ddsman.send_pkt( HWY_odec_op_gen( HWY_IDEC_BRAM, addr, HWY_idec_op_gen( op, param, payload, payload_wid ) ) )

    def HWY_write_Instr_d(self, addr, op, param, payload):
        self.m_ddsman.send_pkt( HWY_odec_op_gen( HWY_IDEC_BRAM, addr, HWY_idec_op_gen( op, param, payload, 9 ) ) )

class ShapedPulse(Pulse):
    def __init__(self, freq, phase, amp, amp_off, vga, pulse_wid, ioupd_to_vga, vga_to_ioupd, end_wait, dds_profile, ddsman, is_func, manage, chan_mask):
        super(ShapedPulse, self).__init__(ddsman, is_func, manage, chan_mask)
        self.m_freq = freq
        self.m_phase = phase
        self.m_amp = amp
        self.amp_off = amp_off
        self.m_vga = vga
        self.m_ioupd_to_vga = ioupd_to_vga
        self.m_vga_to_ioupd = vga_to_ioupd
        self.m_end_wait = end_wait

        self.m_delay = pulse_wid
        assert(dds_profile < 8)
        self.m_dds_profile = dds_profile

        #Specify the TimeSettings associated with wait_for opcodes
        self.m_ioupd_to_vga.set_wf_time()
        m_vga_to_ioupd.set_wf_time()

        #Add this Pulse's associated Settings to a local vector of them,
        #to ease processing later
        self.m_settings.push_back(self.m_freq);
        self.m_settings.push_back(self.m_phase);
        self.m_settings.push_back(self.m_amp);
        self.m_settings.push_back(self.m_amp_off);
        self.m_settings.push_back(self.m_vga);
        self.m_settings.push_back(self.m_delay);
        self.m_settings.push_back(self.m_ioupd_to_vga);
        self.m_settings.push_back(self.m_vga_to_ioupd);
        self.m_settings.push_back(self.m_end_wait);

        # Create a TimeSetting that can be used by other Pulses to copy this one's length
        # (not actually used by the ShapedPulse though)
        extra_time = self.m_ioupd_to_vga.get_time() + self.m_vga_to_ioupd.get_time() + self.m_end_wait.get_time()
        self.m_extra_delay = TimeSetting(extra_time, extra_time > HWY_TIMER_MAX_SHORT_DELAY,ddsman)

        if self.m_chan_mask:    # check overlap if the mask is being specified manually
            self.pass_mask_down()
    def check_addr(self, addr):
        return (0<= addr and addr <= 1023)

    def get_ticks(self):
        return self.m_delay.get_time() + self.m_ioupd_to_vga.get_time() + self.m_vga_to_ioupd.get_time() + self.m_end_wait.get_time()

    def send_obj(self):
        ok= not is_sent();
        assert(ok);
        if(not ok):
            return -1;
        self.set_addr_of_children();
   
        #Check that frequency, amplitude and phase are adjacently spaced
        #in BRAM, to allow SPI burst instruction to read them in one go
        assert(self.m_freq.get_addr() + 2 == self.m_phase.get_addr());
        assert(self.m_phase.get_addr() + 1 == self.m_amp.get_addr());
        #Check that the 'immediate' timer words are in the lower 64
        #addresses of the timer BRAM
        assert(self.m_vga_to_ioupd.get_addr() < 64);
        assert(self.m_ioupd_to_vga.get_addr() < 64);
   
        addr = self.m_addr; # start address
   
        #Start writing ShapedPulse out
        self.HWY_write_Instr_d(addr, HWY_LOAD_SPI, 0x03, self.m_freq.get_addr()); # fill SPI regs
        addr = addr + 1
        self.HWY_write_Instr_d(addr, HWY_SET_VGA, 0x01, self.m_vga.get_addr()); # fill VGA regs
        addr = addr + 1
   
        #wait for VGA to finish, if it's still downramping from previous
        #pulse (finish will coincide with an IOUPDATE from the mvga
        #core, but timer will not be done yet)
        self.HWY_write_Instr(addr, HWY_WAIT_FOR, 0x1c, HWY_TMR_ZERO_ADDR, 6); 
        addr = addr + 1
   
        #no operation (needed to ensure next instruction, HWY_SEND_SPI,
        #is 4 addresses away from the first HWY_LOAD_SPI)
        self.HWY_write_Instr_d(addr, HWY_NOP, 0, 0); 
        addr = addr + 1
   
        #start writing out SPI
        self.HWY_write_Instr_d(addr, HWY_SEND_SPI, 0x00, (0x0e + self.m_dds_profile)); 
        addr = addr + 1
   
        #load zero amp into SPI register
        self.HWY_write_Instr_d(addr, HWY_LOAD_SPI, 0x00, self.m_amp_off.get_addr()); 
        addr = addr + 1
   
        #Wait for timer to finish (from end delay of previous pulse),
        #triggering an IOUPDATE to turn this pulse on at the DDS, and
        #add a short delay for VGA output to start rising after ioupdate
        self.HWY_write_Instr(addr, HWY_WAIT_FOR, 0x12, self.m_ioupd_to_vga.get_addr(), 6);
        addr = addr + 1
   
        #Add a delay for the pulse plateau, at the end of which begin the VGA downramp.
        #First write is in case there is a 'long wait'.
        if (self.m_delay.is_long_time()):
           self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_LONG_TIM|HWY_ST_3, self.m_delay.get_addr()+1);
           addr = addr + 1
       
        self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_VGA, self.m_delay.get_addr());
        addr = addr + 1
   
        #Start writing out SPI again - this time zero value (currently
        #VGA still rising or plateaued)
        self.HWY_write_Instr_d(addr, HWY_SEND_SPI, 0x00, (0x0e + self.m_dds_profile));
        addr = addr + 1
   
       # Short delay to allow VGA to start ramping down, then queue for
       # IOUPDATE to happen. Do not do an IOUPDATE once wait is
       # finished; this is handled by the VGA core.
        self.HWY_write_Instr(addr, HWY_WAIT_FOR, 0x1a, self.m_vga_to_ioupd.get_addr(), 6);
        addr = addr + 1
   
        if(true):
        #Post-pulse wait; queue an ioupdate to occur at the end (to
        #activate whatever initial setting is loaded by the next pulse)
            if (m_end_wait.is_long_time()):
                self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_LONG_TIM|HWY_ST_3, self.m_end_wait.get_addr()+1);
                addr = addr + 1
            self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_IOUPDATE, self.m_end_wait.get_addr());
            addr = addr + 1
    
        #Final addr value is next free BRAM location. Check to make sure
        #the BRAM has not been overflowed. There must be one more free
        #BRAM location for 'function' to return, if implemented at a
        #higher level; so this must be 1023 or less.  @todo this is
        #redundant with existing assert in the constructor; keep only
        #one - or implement at a higher level.
        assert(addr <= 1023);
        self.m_sent = True;
        return addr;

class CfrPulse(Pulse):
    def __init__(self, cfr_addr, cfr, spi_wait, ddsman, manage = True, chan_mask = 0):
        super(CfrPulse,self).__init__(ddsman, False, manage, chan_mask)
        self.m_cfr_addr = cfr_addr
        self.m_cfr = cfr
        self.m_delay = spi_wait
        assert( (self.m_cfr_addr < 0xff) and ( (self.m_cfr_addr & 0x7f)<= 0x16 ) )
        assert( not self.m_delay.is_long_time() )   # make sure time delay is short
        self.m_settings.append(cfr)
        self.m_settings.append(self.m_delay)

        if self.m_chan_mask:
            self.pass_mask_down()

    def send_obj(self):
        ok = not self.is_sent()
        assert(ok)
        if not ok:
            return -1
        addr = self.m_addr
        self.HWY_write_Instr_d( addr, HWY_LOAD_SPI, HWY_LS_BURST_RD_2, self.m_cfr.get_addr() ) # set SPI buffer   FIXME: in the C++ code, self.HWY_write_Instr_d is a macro, but here it's not defined yet. Line 270 in hwy_pulse.cpp 
        addr = addr + 1
        # should always be a reasonably short time delay, not much longer than the time taken to transfer setting over SPI; set IOIPDATE to pulse
        self.HWY_write_Instr_d( addr, HWY_SET_TIM, HWY_ST_IOUPDATE, self.m_delay.get_addr() )
        addr = addr + 1
        self.HWY_write_Instr_d( addr, HWY_SEND_SPI, 0x00, self.m_cfr_addr) # write out SPI buffer to DDS
        addr = addr + 1

        assert(addr <= 1023)
        self.m_sent = True
        return addr


class StartPulse(Pulse):
    def __init__(self, start_wait, trig, ddsman, manage=True, chan_mask=0):
        super(StartPulse, self).__init__(ddsman, False, manage, chan_mask)
        self.m_trig = trig
        self.m_delay = start_wait
        assert(self.m_delay.get_time() >= HWY_SPI_72_TRANSFER_TIME)
        self.m_settings.append(self.m_delay)
        if self.m_chan_mask:
            self.pass_mask_down()

    def get_ticks(self):
        return self.m_delay.get_time()

    def send_obj(self):
        ok = not self.is_sent()
        assert(ok)
        if not ok:
            return -1
        addr = self.m_addr
        long_time = self.m_delay.is_long_time()

        # Set timer, with/without a reset initially
        if self.m_trig:
            if long_time:
                self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_LONG_TIM|HWY_ST_RESET_TMR|HWY_ST_3, self.m_delay.get_addr()+1)
                addr = addr + 1
                self.HWY_write_Instr_d(addr, HWY_SET_TIM,  HWY_ST_3, self.m_delay.get_addr() )
                addr = addr + 1
            else:
                self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_RESET_TMR|HWY_ST_3, self.m_delay.get_addr() )
                addr = addr + 1
        else:
            if long_time:
                self.HWY_write_Instr_d(addr, HWY_SET_TIM,  HWY_ST_LONG_TIM|HWY_ST_3, self.m_delay.get_addr()+1 )
                addr = addr + 1
            self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_IOUPDATE, self.m_delay.get_addr())
            addr = addr + 1

        self.m_sent = True
        assert(addr <= 1023)
        return addr


class EdgePulse(Pulse):
    def __init__(self,freq, phase, amp, on_delay, dds_profile, ddsman, is_func=True, manage=True, chan_mask=0):
        super(EdgePulse,self).__init__(ddsman, is_func, manage, chan_mask)
        self.m_freq = freq
        self.m_phase = phase
        self.m_amp = amp
        self.m_delay = on_delay
        assert(dds_profile < 8)
        self.m_dds_profile = dds_profile
        self.m_settings.append(freq)
        self.m_settings.append(phase)
        self.m_settings.append(amp)
        self.m_settings.append(on_delay)
        if self.m_chan_mask:
            self.pass_mask_down()

    def check_addr(self, addr):
        return 0<=addr and addr<=1023

    def get_ticks(self):
        return self.m_delay.get_time()

    def send_obj(self):
        ok = not is_sent()
        assert(ok)
        if not ok:
            return -1
        self.set_addr_of_children()

        # Check that freq, amp and phase are adjacently spaced in BRAM, to allow SPI burst instruction to read them in one go
        assert(self.m_freq.get_addr() + 2 == self.m_phase.get_addr())
        assert(self.m_phase.get_addr() + 1 == self.m_amp.get_addr())

        addr = self.addr # start address
        # -------- Write stuff out
        # Initial edge turn-on delay
        if self.m_delay.is_long_time():
            self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_LONG_TIM|HWY_ST_3, self.m_delay.get_addr()+1)
            addr = addr + 1
        self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_IOUPDATE, self.m_delay.get_addr())
        addr = addr + 1
        # Fill FPGA SPI regs from BRAM settings
        self.HWY_write_Instr_d(addr, HWY_LOAD_SPI, HWY_LS_BURST_RD_4, self.m_freq.get_addr())
        addr = addr + 1
        # Write FPGA SPI regs to DDS
        self.HWY_write_Instr_d(addr, HWY_SEND_SPI, 0x00, (0x0e + self.m_dds_profile))
        addr = addr + 1

        # Final addr value is next free BRAM location. Check to make sure the BRAM has not been overflowed. 
        # There must be one more free BRAM location for 'function' to return, if implemented at a higer level;
        # so this must be 1023 or less.
        assert(addr<=1023)
        self.m_sent = True

        return addr


class CapPulse(Pulse):
    def __init__(self, *args, **kwds):
        if len(args) == 9:
            freq_ri = args[0]
            phase_ri = args[1]
            amp_ri = args[2]
            fa_delay = args[3]
            dds_profile = args[4]
            ddsman = args[5]
            is_func = args[6]
            manage = args[7]
            chan_mask = args[8]

            freq_fa = ddsman.m_off_freq
            phase_fa = ddsman.m_off_ph
            amp_fa = ddsman.m_off_amp
            ri_delay = ddsman.m_profile_time

        if len(args) == 13:
            freq_ri = args[0]
            freq_fa = args[1]
            phase_ri = args[2]
            phase_fa = args[3]
            amp_ri = args[4]
            amp_fa = args[5]
            ri_delay = args[6]
            fa_delay = args[7]
            dds_profile = args[8]
            ddsman = args[9]
            is_func = args[10]
            manage = args[11]
            chan_mask = args[12]

        super(CapPulse, self).__init__(ddsman, is_func, manage, chan_mask)
        self.m_freq_ri = freq_ri
        self.m_freq_fa = freq_fa
        self.m_phase_ri = phase_ri
        self.m_phase_fa = phase_fa
        self.m_amp_ri = amp_ri
        self.m_amp_fa = amp_fa

        self.m_delay = fa_delay
        self.m_extra_delay = ri_delay
        assert(dds_profile < 8 )
        self.m_dds_profile = dds_profile

        self.m_settings.append(freq_ri)
        self.m_settings.append(phase_ri)
        self.m_settings.append(amp_ri)
        self.m_settings.append(ri_delay)

        self.m_settings.append(freq_fa)
        self.m_settings.append(phase_fa)
        self.m_settings.append(amp_fa)
        self.m_settings.append(fa_delay)

        if self.m_chan_mask:
            self.pass_mask_down()


    def check_addr(self, addr):
        return 0<=addr and addr <= 1023

    def get_ticks(self):
        return self.m_delay.get_time() + self.m_extra_delay.get_time()

    def send_obj(self):
        ok = not self.is_sent()
        assert(ok)
        if not ok:
            return -1

        self.set_addr_of_children()

        # Check that freq, amp and phase are adjacently spaced in BRAM, to allow SPI burst instruction to read them in one go
        assert(self.m_freq_ri.get_addr() + 2 == self.m_phase_ri.get_addr())
        assert(self.m_freq_fa.get_addr() + 2 == self.m_phase_fa.get_addr())
        assert(self.m_phase_ri.get_addr() + 1 == self.m_amp_ri.get_addr())
        assert(self.m_phase_fa.get_addr() + 1 == self.m_amp_fa.get_addr())

        addr = self.m_addr # start address

        # -------Write stuff out
        # Initial cap turn-on delay
        if self.m_extra_delay.is_long_time():
            self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_LONG_TIM|HWY_ST_3, self.m_extra_delay.get_addr()+1)
            addr = addr + 1
        self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_IOUPDATE, self.m_extra_delay.get_addr())
        addr = addr + 1
        # Fill FPGA SPI regs from BRAM settings, turn-on
        self.HWY_write_Instr_d(addr, HWY_LOAD_SPI, HWY_LS_BURST_RD_4, self.m_freq_ri.get_addr())
        addr = addr + 1
        # Write FPGA SPI regs to DDS
        self.HWY_write_Instr_d(addr, HWY_SEND_SPI, 0x00, (0x02 + self.m_dds_profile))
        addr = addr + 1

        # Cap turn-off delay
        if self.m_delay.is_long_time():
            self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_LONG_TIM|HWY_ST_3, self.m_delay.get_addr())
            addr = addr + 1
        self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_IOUPDATE, self.m_delay.get_addr())
        addr = addr + 1

        # Fill FPGA SPI regs from BRAM settings, turn-off
        self.HWY_write_Instr_d(addr, HWY_LOAD_SPI, HWY_LS_BURST_RD_4, self.m_freq_fa.get_addr())
        addr = addr + 1

        # Write FPGA SPI regs to DDS
        self.HWY_write_Instr_d(addr, HWY_SEND_SPI, 0x00, (0x0e + self.m_dds_profile))
        addr = addr + 1

        assert(addr <= 1023)
        self.m_sent = True
        return addr


class WaitPulse(Pulse):
    def __init__(self, delay, ddsman, is_func=True, manage=True, chan_mask=0):
        super(WaitPulse, self).__init__(ddsman, is_func, manage, chan_mask)
        self.m_delay = delay
        self.m_settings.append(self.m_delay)
        if self.m_chan_mask:
            self.pass_mask_down()

    def get_ticks(self):
        return self.m_delay.get_time()

    def send_obj(self):
        ok = not self.is_sent()
        assert(ok)
        if not ok:
            return -1
        self.set_addr_of_children()

        addr = self.m_addr
        if self.m_delay.is_long_time():
            self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_LONG_TIM|HWY_ST_3, self.m_delay.get_addr()+1)
            addr = addr + 1

        self.HWY_write_Instr_d(addr, HWY_SET_TIM, HWY_ST_3, self.m_delay.get_addr())
        addr = addr + 1

        assert(addr<=1023)
        self.m_sent = True

        return addr

        
