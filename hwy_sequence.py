from hwy_hw import *
from hwy_pulse import *

class Sequence(Pulse):
    def __init__(self, ddsman, manage, chan_mask):
        super(Sequence,self).__init__(ddsman, False, manage, chan_mask)
        self.m_pulses = []

    def append(self, pulse):
        self.m_pulses.append(pulse)

        # Make sure Sequence-wide channel mask is already defined
        assert(self.m_chan_mask)
        pulse.pass_mask_down(self.m_chan_mask)
        
    def pass_mask_down(self, *args, **kwds):
        if len[args] == 1:
            chan_mask = args[0]
            assert(0)
        if len[args] == 0 or args is None:
            for i in self.m_pulses:
                i.add_to_chan_mask(self.m_chan_mask)

    def get_ticks(self):
        return 0

 

class InitSequence(Sequence):
    def __init__(self, ddsman, trig_loop,manage, chan_mask):
        super(InitSequence,self).__init__(ddsman, manage, chan_mask)
        self.m_trig_loop = trig_loop

    def send_obj(self):
        addr = self.m_ddsman.get_free_addr('idec_br', self.m_chan_mask)
        assert(addr is not None)

        # Reset VGA before doing anything else
        self.HWY_write_Instr_d(addr, HWY_SET_VGA, HWY_SV_RESET_VGA, 0)
        addr = addr + 1

        for i in self.m_pulses:
            # Set up the addresses of each Pulses's member settings
            i.set_addr_of_children()
            if X86_EMULATION:
                print "# Writing init pulse. Address: %d, sent: %d\n"%(i.has_addr(),i.is_sent() )
            # send initialisation Pulses (eg CfrPulses); check they're inline (though they dont't have to be)
            assert(not i.is_func())
            # sned_obj returns the next free address, after sending the object. 
            # Each one is inlien, so no global send is required
            # -- sned_obja() is sufficient
            addr = i.send_obja(addr)
            if X86_EMULATION:
                print "    # End init pulse\n"

        # Reset VGA
        self.HWY_write_Instr_d(addr, HWY_SET_VGA, HWY_SV_RESET_VGA, 0)
        addr = addr + 1
        # -----------------Trigger--------------------
        # Write a trigger (triggers all the Sequence's channels)
        # then wait nominal amount after trigger
        self.m_ddsman.set_return_addr(addr, self.m_chan_mask)
        if self.m_trig_loop:
            self.HWY_write_Instr(addr, HWY_WAIT_FOR, HWY_WF_WAIT_TRIG, HWY_TMR_ZERO_ADDR, 6)
            addr = addr + 1
        self.m_ddsman.set_free_addr(addr, 'idec_br', self.m_chan_mask)
        return 0


class LoopSequence(Sequence):
    def __init__(self, ddsman, manage, chan_mask):
        super(LoopSequence,self).__init__(ddsman, manage, chan_mask)

    def send_obj(self):
        addr = self.m_ddsman.get_free_addr('idec_br', self.m_chan_mask)
        if X86_EMULATION:
            print "ADDR: %d\n"%addr

        for i in self.m_pulses:
            # Set up each Pulse's Settings' addresses
            i.set_addr_of_children()
            if X86_EMULATION:
                print "    # Writing loop pulse. Function: %d, address: %d, sent: %d\n"%(i.is_func(), i.get_addr(), i.is_sent())
            if i.is_func():  # Pulse is function-like; need to branch to it
                if i.has_addr():    # Pulse address is already set; use it to branch to
                    pulse_return_addr = i.get_addr()
                else: # Pulse has no address yet
                    func_addr = self.m_ddsman.get_free_addr(idec_fn_br, i.get_chan_mask())
                    i.set_addr(func_addr)
                    pulse_return_addr = func_addr

                self.HWY_write_Instr(addr, HWY_BRANCH, 0x0, pulse_return_addr, 10) # branch to pulse
                addr = addr + 1
                if not i.is_sent():
                    func_addr = self.m_ddsman.send(i) # send Pulse, if not already set

                    self.HWY_write_Instr(func_addr, HWY_BRANCH, HWY_BR_RETURN, 0, 10) # return to incline code
                    func_addr = func_addr + 1
                    # update free function address in RAM
                    self.m_ddsman.set_free_addr(func_addr, idec_fn_br, i.get_chan_mask())
                    # return channel mask back to this Sequence's
                    self.m_ddsman.send_control(self.m_chan_mask)
            else: # No branch necessary; write pulse (or loop sequence) inline
                addr = i.send_obja(addr)

            if X86_EMULATION:
                print "    # End loop pulse\n"

            # Branch back to the loop start
            self.m_end_return_addr = self.m_ddsman.get_return_addr(self.m_chan_mask)
            self.HWY_write_Instr(addr, HWY_BRANCH, 0x0, self.m_end_return_addr,10)
            addr = addr + 1

            # Update free space
            self.m_ddsman.set_free_addr(addr, 'idec_br',self.m_chan_mask)
            return 0

class SubLoop(Sequence):
    def __init__(self, loops, ddsman, manage=True, chan_mask=0):
        super(SubLoop,self).__init__(ddsman, manage, chan_mask)
        self.m_loops = loops
        self.m_fifo_pops_per_loop = 0
        assert(loops<4096) # 12-bit register on Mildown board
    def set_addr_of_children(self):
        for i in self.m_pulses:
            i.set_addr_of_children()

    def pass_mask_down(self, chan_mask):
        self.add_to_chan_mask(chan_mask)
        #self.pass_mask_down()
        for i in self.m_pulses:
            i.add_to_chan_mask(self.m_chan_mask)

    def dyn_update(self,s):
        s.register_for_fifo()
        self.increment_pops_per_loop(s)

    def increment_pops_per_loop(self,s):
        self.m_fifo_pops_per_loop = self.m_fifo_pops_per_loop + s.size()
        assert(self.m_fifo_pops_per_loop < HWY_MAX_FIFO_POPS_PER_LOOP)

    def get_ticks(self):
        return 0

    def send_obja(self, addr):
        if X86_EMULATION:
            print "   Subloop Addr: %d\n"%addr
        # Initialise loop register
        self.HWY_write_Instr(addr,HWY_SET_REG, HWY_REG0, self.m_loops,12)
        addr = addr + 1
        loop_return_addr = addr

        # Decrement loop register
        self.HWY_write_Instr(addr, HWY_WAIT_FOR, HWY_WF_DECREMENT_REG0|HWY_WF_3, HWY_TMR_ZERO_ADDR,6)
        addr = addr + 1
        for i in self.m_pulses:
            # Set up each Pulse's Settings' addresses
            i.set_addr_of_children()
            if X86_EMULATION:
                print "    # SUBLOOP: Writing subloop pulse. Function: %d, address: %d, sent: %d\n"%(i.is_func(), i.get_addr(), i.is_sent())
            if i.is_func(): # Pulse is function-like, need to branch to it
                if i.has_addr(): # Pulse address is already set; use it to branch to
                    pulse_return_addr = i.get_addr()
                else: # Pulse has no address yet
                    func_addr = self.m_ddsman.get_free_addr(idec_fn_br, self.m_chan_mask)
                    i.set_addr(func_addr)
                    pulse_return_addr = func_addr

                self.HWY_write_Instr(addr, HWY_BRANCH, 0x0, pulse_return_addr, 10) # Branch to pulse
                addr = addr + 1
                if not i.is_sent():
                    func_addr = self.m_ddsman.send(i) # send Pulse, if not already set
                    self.HWY_write_Instr(func_addr, HWY_BRANCH, HWY_BR_RETURN, 0, 10) # return to inline code
                    func_addr = func_addr + 1

                    # update free function address in RAM
                    self.m_ddsman.set_free_addr(func_addr, idec_fn_br, i.get_chan_mask())

                    # return channel mask back to this Sequence's
                    self.m_ddsman.send_control(m_chan_mask)
            else:
                # No branch necessary; write pulse (or loop sequence inline)
                addr = i.send_obja(addr)
            if X86_EMULATION:
                print "    # SUBLOOP: End subloop pulse\n"
        # pop from update FIFO
        self.HWY_write_Instr_d(addr, HWY_ALT_TIM, 0, self.m_fifo_pops_per_loop)
        addr = addr + 1

        # Branch back to the loop start, if loop register is nonzero
        self.HWY_write_Instr(addr, HWY_BRANCH_NZ, HWY_REG0, loop_return_addr,12)
        addr = addr + 1

        return addr

    def send_obj(self):
        assert(0)



