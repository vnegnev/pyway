from hwy_hw import *
from hwy_selftest import *
from hwy_setting import *
from hwy_pulse import *
from hwy_boss import *
import hwy_util
import sys

def main(args):
    write_lut = True
    boss_test = 0
    dds_card = 0

    
    if len(args) < 3:
        print "1sd number: 0/1 whether to write LUT out.\n"
        print "2nd number: 0/1/2 whether to do boss selftest with Edges (1), Caps (2) or just manual Edge selftest (0).\n"
        print   "3rd number: 0-3 which DDS to save binary data/printouts to.\n"
        return 0

    else:
        write_lut = args[1]
        boss_test = args[2]
        if len(args) == 3:
            dds_card = args[3]
    hwy_util.hwy_dds_card = dds_card 

    print "### DDS test: "
    if boss_test:
        print "boss, "
    else:
        print "edge,"

    if write_lut:
        print "LUT, "
    else:
        print "no LUT, "

    print "DDS card %d\n"%dds_card

    if write_lut:
        fp = open("hiway_slave.bin", 'w')
    else:
        fp = open("hiway_slave_nolut.bin", 'w')

    if not fp:
        return -1

    # No arguments -> no LUT
    # Any arguments -> with LUT
    loop_trig = True
    if boss_test == 1:
        ddsm = Boss(fp)
        m = boss_edge_selftest(ddsm, write_lut, True, dds_card*4)
    elif boss_test == 2:
        ddsm = Boss(fp)
        m = boss_cap_selftest(ddsm, write_lut, True, dds_card*4)
    else:
        ddsm = DdsManager(fp)
        m = edge_sequence_selftest(ddsm, True, write_lut, loop_trig, True)

    print "Selftest returned ... %d \n"%m
    
    fp.close()
    return 0



    



if __name__ == '__main__':
    main(sys.argv)
