from hwy_hw import *
from hwy_setting import *
from hwy_pulse import *
from hwy_sequence import *


class HwyManager():
    def __init__(self, hwy_base_addr):
        self.m_dds_spi_ram_addr = [ [ None for j in range(0,HWY_DDS_CHANNELS) ] for i in range(0,HWY_DDS_CARDS) ]
        self.m_dds_par_ram_addr = [ [ None for j in range(0,HWY_DDS_CHANNELS) ] for i in range(0,HWY_DDS_CARDS) ]
        self.m_timer_ram_addr = [ [ None for j in range(0,HWY_DDS_CHANNELS) ] for i in range(0,HWY_DDS_CARDS) ]
        self.m_vga_ram_addr = [ [ None for j in range(0,HWY_DDS_CHANNELS) ] for i in range(0,HWY_DDS_CARDS) ]
        self.m_idecoder_ram_addr = [ [ None for j in range(0,HWY_DDS_CHANNELS) ] for i in range(0,HWY_DDS_CARDS) ]

        self.m_ddsm = DdsManager(hwy_base_addr)

        # Initialise all the RAM addresses
        self.init_ram(self.m_dds_spi_ram_addr)
        self.init_ram(self.m_dds_par_ram_addr)
        self.init_ram(self.m_timer_ram_addr)
        self.init_ram(self.m_vga_ram_addr)
        self.init_ram(self.m_idecoder_ram_addr)

    def init_ram(self, addr_array):
        for card in range(0, HWY_DDS_CARDS):
            for chan in range(0, HWY_DDS_CHANNELS):
                addr_array[card][chan] = 0

    def freq(self, tw):
        return HwyFreq(tw)

    def amp(self,tw):
        return HwyAmp(tw)

    def time(self, tw):
        return HwyTime(tw)

    def pulse(self, t_wait, t_on, f_on, f_off, a_on, a_off, ph_on, ph_off, name):
        return HwyPulse(t_wait, t_on, f_on, f_off, a_on, a_off, ph_on, ph_off, name)

    def seq(self, pulses, chan_mask):
        return HwySeq(pulses, chan_mask)


class HwySett():
    def __init__(self):
        self.m_chan_mask = 0

    def add_to_chan_mask(self, chan_mask):
        self.m_chan_mask = self.m_chan_mask|chan_mask


class HwyFreq(HwySett):
    def __init__(self, tw):
        super(HwyFreq,self).__init__()
        self.m_ftw = tw

class HwyAmp(HwySett):
    def __init__(self, tw):
        super(HwyAmp,self).__init__()
        self.m_atw = tw

class HwyPh(HwySett):
    def __init__(self, tw):
        super(HwyPh,self).__init__()
        self.m_ptw = tw

class HwyTime(HwySett):
    def __init__(self, tw):
        super(HwyTime,self).__init__()
        self.m_ticks = tw

class HwyPulse():
    def __init__(self, t_wait, t_on, f_on, f_off, a_on, a_off, ph_on, ph_off, name):
        self.m_t_wait = t_wait
        self.m_t_on = t_on
        self.m_f_on = f_on
        self.m_f_off = f_off
        self.m_a_on = a_on
        self.m_a_off = a_off
        self.m_ph_on = ph_on
        self.m_ph_off = ph_off
        self.m_name = name
        self.m_chan_mask = 0

    def add_to_chan_mask(self, chan_mask):
        self.m_chan_mask = self.m_chan_mask|chan_mask
        self.m_t_wait.add_to_chan_mask(chan_mask)
        self.m_t_on.add_to_chan_mask(chan_mask)
        self.m_f_on.add_to_chan_mask(chan_mask)
        self.m_f_off.add_to_chan_mask(chan_mask)
        self.m_a_on.add_to_chan_mask(chan_mask)
        self.m_a_off.add_to_chan_mask(chan_mask)
        self.m_ph_on.add_to_chan_mask(chan_mask)
        self.m_ph_off.add_to_chan_mask(chan_mask)


class HwySeq():
    def __init__(self, pulses, chan_mask):
        self.m_chan_mask = chan_mask
        self.m_pulses = pulses
        for p in pulses:
            p.add_to_chan_mask(self.m_chan_mask)

