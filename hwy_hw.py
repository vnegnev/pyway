from hwy import * 
#from hwy_hw_h import *
from hwy_setting import *
#from hwy_pulse import *
#from hwy_sequence import *
import hwy_util 


class DdsManager(object):
    def __init__(self, hwy_base_addr):
        self.m_card_mask = 0 
        self.m_msbs = 0
        self.m_msbs_written = False
        self.m_hwy_base_addr = hwy_base_addr
        self.m_dds_chan_masks = [0 for i in range(0,8)]
        self.m_settings = []
        self.m_pulses = []

        self.reset_ram()
        self.init_glob_settings()
        #self.m_seq_return_addr = 

    def reset_ram(self):
         # Initialise all RAM addresses
         self.m_dds_spi_addr = [0 for i in range(0,HWY_DDS_CHAN)]
         self.m_dds_par_addr = [0 for i in range(0,HWY_DDS_CHAN)]
         self.m_timer_addr = [HWY_START_TMR_ADDR for i in range(0,HWY_DDS_CHAN)]
         self.m_timer_wf_addr = [HWY_START_TMR_WF_ADDR for i in range(0,HWY_DDS_CHAN)]
         self.m_vga_addr = [0 for i in range(0,HWY_DDS_CHAN)]
         self.m_idec_addr = [HWY_START_IDEC_ADDR for i in range(0,HWY_DDS_CHAN)]
         self.m_idec_return_addr = [0 for i in range(0,HWY_DDS_CHAN)]
         self.m_idec_fn_addr = [HWY_START_IDEC_FN_ADDR for i in range(0,HWY_DDS_CHAN)]
        

    def init_glob_settings(self):
         # Initialise universally-used Settings, managed by DdsManager (so
         # that they can be used later by things outside the dds::init()
         #method).  These Settings are only transmitted if they're used
         # by user-constructed Pulses.
         self.m_off_freq = SpiFreqSetting(0, self, True, HWY_DDS_ALL_CHANNELS)
         self.m_off_ph =  SpiPhaseSetting(0, self, True, HWY_DDS_ALL_CHANNELS)
         self.m_off_amp = SpiAmpSetting(0, self, True, HWY_DDS_ALL_CHANNELS)
         self.m_profile_time = TimeSetting(162, False,
                                   self, True, HWY_DDS_ALL_CHANNELS)
   
         #Initialise Settings used only by the dds::init() method; thus
         #not managed by DdsManager
         self.m_cfr_time = TimeSetting(106, False,
                           self, False, HWY_DDS_ALL_CHANNELS)
   
        #Initialise universally-used Settings at specific addresses so
        #that WAIT_FOR instructions can use them
         self.m_zero_time = TimeSetting(HWY_TMR_ZERO_ADDR, HWY_TMR_ZERO_LEN, False,
                        self, False, HWY_DDS_ALL_CHANNELS)
         self.m_short_time = TimeSetting(HWY_TMR_SHORT_ADDR, HWY_TMR_SHORT_LEN, False,
                        self, False, HWY_DDS_ALL_CHANNELS)

    def append_setting(self, sett):
         self.m_settings.append(sett)

    def append_pulse(self, p):
        self.m_pulses.append(p)

    def send_settings(self):
        for i in self.m_settings:
            self.send(i)

    def clear_settings(self):
        for i in self.m_settings:
            self.m_settings.pop(i)

    def clear_pulses(self):
        for i in self.m_pulses:
            self.m_pulses.pop(i)

    def delete_glob_settings(self):
        self.m_cfr_time = None
        self.m_zero_time = None
        self.m_short_time = None

    def clear_all(self):
        self.delete_glob_settings()
        self.clear_settings()
        self.clear_pulses()
        self.reset_ram()
        self.init_glob_settings()
  
    def set_card_mask(self,card_mask):
        if card_mask != self.m_card_mask:
            self.m_card_mask = card_mask
            self.m_msbs_written = False
            return True;
  
        return False;
  
    def clear_comms_errors(self,card_mask):
        if X86_EMULATION:
            assert(0)
        else:
            Xil_Out32(self.m_hwy_base_addr + HWY_REG_CLR_OFFSET, card_mask)

    def get_comms_errors(self, card):
        """ Results will be equal to HWY_REG_ERR_XXX_OFFSET.
          eg. for 0 and 1 returns 0xc, 2 and 3 returns 0x10, 4/5: 0x14, 6/7: 0x18.
       """
        err_reg_offset = (3 + (card>>1)) << 2
       # Channel errors stored in 16 MSBs or LSBs?*/
        left_end = (card & 0x01)
        if left_end:
            return Xil_In32(self.m_hwy_base_addr + err_reg_offset) >> 16
        else:
            return Xil_In32(( self.m_hwy_base_addr + err_reg_offset )) & 0xffff

    def set_msb_ctrl(self, msbs):
        if msbs != self.m_msbs:
            self.m_msbs = msbs;
            self.m_msbs_written = False;
            return True;
        return False;

    def write_msbs(self):
        if not self.m_msbs_written:
            if X86_EMULATION:
                hwy_util.HWY_X86_write_msbs((self.m_card_mask << 16) | self.m_msbs)
            else:
                #print self.m_hwy_base_addr
                hwy_util.Xil_Out32(self.m_hwy_base_addr + HWY_REG_MSB_OFFSET, (self.m_card_mask << 16) | self.m_msbs);
        self.m_msbs_written = True;

    #def write_msbs(self, card_mask,  msbs):
    def send_pkt(self, lsbs):
        if X86_EMULATION:
            hwy_util.hwy_lsbs = lsbs
            # Print debugging information to stdout
            hwy_util.HWY_X86_parse_pkt()

            # Only write to binary if the data would go to selected DDS card
            if self.m_card_mask & (0x01 << hwy_util.hwy_dds_card):
                #print self.m_hwy_base_addr, "%04x%08x\n"%(hwy_util.hwy_msbs, hwy_util.hwy_lsbs)
                self.m_hwy_base_addr.write( "%04x%08x\n"%(hwy_util.hwy_msbs, hwy_util.hwy_lsbs))
        else:
            hwy_util.Xil_Out32(self.m_hwy_base_addr + HWY_REG_LSB_OFFSET, lsbs)
       
    def send_control(self, chan_mask, force_updates=False,  idec_rst=False,  update_ctrl_word=False,  trig=False,  ctrl_word=0):
        card_mask = 0
        dds_card_bit = 0
        required_chan_mask = 0
        hwy_sends = 0
        # Calculate card_mask to reflect what card mask must be written to hiway
        for k in range(0,8):
            dds_card_bit = 1<<k
            required_chan_mask = (chan_mask >> (4*k)) & 0xf
            if required_chan_mask:
                card_mask = card_mask|dds_card_bit

                # an update to the DDS is required
            if ( required_chan_mask and ((required_chan_mask != self.m_dds_chan_masks[k]) or force_updates)):
                # send only to one DDS
                self.set_card_mask(dds_card_bit)
                self.set_msb_ctrl(HWY_odec_ctrl_gen(required_chan_mask, idec_rst, trig, ctrl_word, update_ctrl_word))
                self.write_msbs()
                # send the core's data to the DDSes
                self.send_pkt(0)
                hwy_sends = hwy_sends + 1
                self.m_dds_chan_masks[k] = required_chan_mask
         # clear MSB control word, and set card mask - only gets written if there has been a change (this is handled in write_msbs)
        self.set_card_mask(card_mask)
        self.set_msb_ctrl(0)
        self.write_msbs()
        return hwy_sends
           
    def halt_and_reset_dds(self):
        return self.send_control(HWY_DDS_ALL_CHANNELS, True, True, True, False, HWY_CTRL_DDS_MASTER_RESET|HWY_CTRL_MTIMER_RESET|HWY_CTRL_VGA_RESET_STATE_HI)
    def halt_dds(self):
        return self.send_control(HWY_DDS_ALL_CHANNELS, True, True, True, False, HWY_CTRL_MTIMER_RESET|HWY_CTRL_VGA_RESET_STATE_HI )
    def run_dds(self):
        return self.send_control(HWY_DDS_ALL_CHANNELS, True, False, True, False, HWY_CTRL_VGA_RESET_STATE_HI)

    def send(self, *args):
        if len(args) == 2:
            obj = args[0]
            addr = args[1]
            assert(not obj.is_sent())
            if obj.is_sent():
                return -1 # object already set
            self.send_control(obj.get_chan_mask)    # set up DDS card masks for all DDS odecoders
            return obj.send_obja(addr)
        if len(args) == 1:
            obj = args[0]
            assert(not obj.is_sent())
            if obj.is_sent():
                return -1 # object already set
            self.send_control(obj.get_chan_mask())    # set up DDS card masks for all DDS odecoders
            return obj.send_obj()


    #def send(self,  obj,  addr):
    #    assert(not obj.is_sent())
    #    if obj.is_sent():
    #        return -1 # object already set
    #    self.send_control(obj.get_chan_mask)    # set up DDS card masks for all DDS odecoders
    #    return obj.send_obja(addr)

    #def send(self, obj):
    #    assert(not obj.is_sent())
    #    if obj.is_sent():
    #        return -1 # object already set
    #    self.send_control(obj.get_chan_mask)    # set up DDS card masks for all DDS odecoders
    #    return obj.send_obj()

    def send_to_fifo(self, obj):
        self.send_control(obj.get_chan_mask())   # set up DDS card masks for all DDS odecoders
        return obj.send_to_fifo()

    def send_sequence(self,  seq):
        return 0


    def get_free_addr(self, br,  chan_mask):
        if br == 'spi_br':
            free_addrs = self.m_dds_spi_addr
        elif br == 'par_br':
            free_addrs = self.m_dds_par_addr
        elif br == 'tmr_br':
            free_addrs = self.m_timer_addr
        elif br == 'tmr_wf_br':
            free_addrs = self.m_timer_wf_addr
        elif br == 'vga_br':
            free_addrs = self.m_vga_addr
        elif br == 'idec_br':
            free_addrs = self.m_idec_addr
        elif br == 'idec_return_br':
            free_addrs = self.m_idec_return_addr
        elif br == 'idec_fn_br':
            free_addrs = self.m_idec_fn_addr
        else:
            assert(0)
            
        #Figure out which channels have a free address. Very dumb system
        # whereby the free address is just the maximum of the free
        # addresses of all the channels concerned.
        max_free_addr = 0
        current_chan = 0x00000001
        for k in range(0,HWY_DDS_CHAN):
            if current_chan & chan_mask:
                if max_free_addr < free_addrs[k]:
                    max_free_addr = free_addrs[k]
            current_chan = current_chan<<1
        return max_free_addr

    def set_free_addr(self, addr,  br,  chan_mask):
        if br == 'spi_br':
            free_addrs = self.m_dds_spi_addr
        elif br == 'par_br':
            free_addrs = self.m_dds_par_addr
        elif br == 'tmr_br':
            free_addrs = self.m_timer_addr
        elif br == 'tmr_wf_br':
            free_addrs = self.m_timer_wf_addr
        elif br == 'vga_br':
            free_addrs = self.m_vga_addr
        elif br == 'idec_br':
            free_addrs = self.m_idec_addr
        elif br == 'idec_return_br':
            free_addrs = self.m_idec_return_addr
        elif br == 'idec_fn_br':
            free_addrs = self.m_idec_fn_addr
        else:
            assert(0)

        #Set the channels specified by chan_mask to have the free
            # address specified by addr.
        current_chan = 0x00000001
        for k in range(0,HWY_DDS_CHAN):
            if current_chan & chan_mask:
                free_addrs[k] = addr
            current_chan = current_chan<<1

    def allocate_free_addr(self, br,  locs,  chan_mask):
        free_addr = self.get_free_addr(br, chan_mask)
        self.set_free_addr(free_addr+locs, br, chan_mask)
        return free_addr;

    def get_return_addr(self, chan_mask):
        return self.get_free_addr('idec_return_br', chan_mask)
    def set_return_addr(self, addr, chan_mask ):
        return self.set_free_addr(addr, 'idec_return_br', chan_mask)

# class DdsBramObject is put into hwy_hw0.py because of circular referencing
"""class DdsBramObject():
    def __init__(self, ddsman, chan_mask=0, chan_mask_mutable=True):
        self.m_ddsman = ddsman
        self.m_chan_mask = chan_mask;
        self.m_addr = -1;
        self.m_sent = False;
        self.m_chan_mask_mutable = chan_mask_mutable;
        self.m_settings = []

        if chan_mask:
            self.m_chan_mask_mutable = False

    def has_addr(self):
        return self.m_addr != (-1)

    def check_addr(self, addr):
        return (0 <= addr and addr < 512)

    def is_sent(self):
        return self.m_sent

    def send_obja(self,addr):
        if self.has_addr(): #Ensure that, if an address has been assigned
                         # already, the object has previously been sent
                         # out.
            assert(is_sent())
            self.m_sent = False
   
        self.m_addr = self.addr
       #unsigned r = send_obj();
       #m_addr = static_cast<u16>(-1);
       #return r;
        return self.send_obj()

    def send_obj(self):  # declared as virtual
        pass

    def get_addr(self):
        return self.m_addr

    def set_addr(addr):
        ok = self.check_addr(addr) and not self.has_addr()  #check_addr() is pulled from children
        assert(ok)
        if(not ok):
            return -1
        self.m_addr = addr;
    #    m_sent = false; // need to resend the Setting
        return 0;
    
    def get_chan_mask():
        return self.m_chan_mask

    def chan_mask_mutable():
        return self.m_chan_mask_mutable

    def add_to_chan_mask(chan_mask):
        # higher-level Pulse (or whatever) needs to check this out itself
        assert(m_chan_mask_mutable);
        
        if (self.m_chan_mask != chan_mask):
            self.m_sent = False;
            self.m_chan_mask |= chan_mask;
"""





def HWY_idec_op_gen(op, param, payload, payload_wid):
    assert(payload < (1 << payload_wid));
    assert(param < (1 << (14-payload_wid)));
    #Note: param here is shifted depending on payload width - this
    # is inconsistent with the idecoder.v definition.
    return (op << 14) | (param << payload_wid ) | payload;
   
def HWY_odec_op_gen( bram,  addr,  field):
    return (bram << 28) | (addr << 18) | field;
    
def HWY_odec_ctrl_gen(chan_mask, idec_rst, trig, ctrl_word, update_ctrl_word=True):
    assert (chan_mask > 0 and chan_mask < 16)
    return (1 << 15) | (update_ctrl_word << 14) | (idec_rst << 13) | (trig << 12) | (chan_mask << 8) | (ctrl_word)
    
def freq_to_ftw(freq):
    return int(freq*4294967296.0/1000)
    
def amp_to_atw(amp):
    return int(amp*163.83) # 16383 corresponds to max amp for input of 100
    
def phase_to_ptw(phase):
    return int((phase*65536.0/360) % 65536) 
    
def time_to_ticks(microsec):
    return long(microsec*1000.0/8 - 1)# TODO: avoid floor error!


