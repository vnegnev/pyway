from hwy_setting import *
from hwy_pulse import *
from hwy_sequence import *

# For simulating the hiway core's LSB and MSB registers, and DDS card being used
hwy_lsbs = None
hwy_msbs = None
hwy_dds_card = 0

def Xil_Out32(addr, data):
    print "addr 0x%0x, data 0x%0x\n"%(addr, data)


def HWY_X86_write_msbs(msbs):
    print "------------------------------------------------"
    print "(MSBs: card mask: 0x%0x)\n"%((msbs >> 16)& 0xffff)
    global hwy_msbs
    hwy_msbs = msbs & 0xffff
    return 0


def HWY_X86_parse_pkt():
    lsbs = hwy_lsbs
    msbs = hwy_msbs
    assert(msbs is not None)
    assert(lsbs is not None)
    dds_card = hwy_dds_card

    ctrl_instr = msbs & 0x8000
    apply_ctrl_word = msbs & 0x4000
    idec_reset = msbs & 0x2000
    manual_trigger = msbs & 0x1000

    dds_chan_mask = (msbs >> (8 + 4*dds_card) ) & 0xf
    ctrl_word = msbs & 0xff
    mdds_ser = 0

    # BRAM bits/flags
    bram_index = (lsbs >> 28) & 0x7
    bram_addr = (lsbs >> 18) & 0x3ff # up to 1023
    bram_data = lsbs & 0x3ffff
    bram_fifo = lsbs & 0x80000000
    bram_instr = 0

    print "0x%04x_%08x : "%(msbs,lsbs),
    if ctrl_instr:
        print "control, ",
        if apply_ctrl_word:
            print "A",
        if idec_reset:
            print "R",
        if manual_trigger:
            print "T",
        print " chan mask 0x%01x" %dds_chan_mask,
        if apply_ctrl_word:
            print " ctrl word 0x%02x:"%ctrl_word,
            if ctrl_word & 0x1:
                print " mtimer_rst, ",
            if ctrl_word & 0x2:
                print " dds_master_rst, ",
            if ctrl_word & 0x4:
                print " dds_ext_pwr_dwn, ",
            if ctrl_word & 0x8:
                print " vga_ramp_rst->hi, ",
    else:
        if bram_fifo:
            print "FIFO, ",
        else:
            print "bram, ",
        if bram_index == HWY_IDEC_BRAM:
            print "idec    : ",
            bram_instr = 1
        elif bram_index == HWY_MTIM_BRAM:
            print "mtim    : ",
        elif bram_index == HWY_MDDS_S_BRAM:
            print "mdds ser: ",
            mdds_ser = 1
        elif bram_index == HWY_MDDS_P_BRAM:
            print "mdds par: ",
        elif bram_index == HWY_MVGA_BRAM:
            print "mvga    : ",
        elif bram_index == HWY_MVGA_L_BRAM:
            print "mvga lut: ",
        else:
            print "UNKNOWN : ",
            print lsbs,bram_index
            #raw_input()

        print "addr %4d, data 0x%05x (%6d) "%( bram_addr, bram_data, bram_data),

        # idecoder instruction
        if bram_instr:
            idec_opcode = bram_data >> 14

            # Note : this does not precisely correspond to the way param is assembled in HWY_idec_op_gen().
            idec_param = (bram_data >> 9 )& 0x1f
            idec_payload = bram_data & 0x1ff
            idec_payload_10b = bram_data & 0x3ff
            idec_payload_12b = bram_data & 0xfff
            op_wait_for = 0
            op_branch = 0
            op_set_reg = 0
            print "op 0x%01x, param 0x%02x, payload 0x%03x, "%( idec_opcode, idec_param, idec_payload),

            if idec_opcode == HWY_NOP:
                print "nop   ",
            elif idec_opcode == HWY_LOAD_SPI:
                print "load_spi ",
                if idec_param & 0x3:
                    print " [burst read %d] "%((idec_param & 0x3)+1),
            elif idec_opcode == HWY_SEND_SPI:
                print "send_spi ",
            elif idec_opcode == HWY_SET_VGA:
                print "set_vga ",
                if idec_param & 0x1:
                    print " [ioupdate after fall] ",
                if idec_param & 0x2:
                    print " [ioupdate after rise] ",
            elif idec_opcode == HWY_ALT_VGA:
                print "alt_vga ",
            elif idec_opcode == HWY_SET_PAR:
                print "set_par   ",
            elif idec_opcode == HWY_ALT_PAR:
                print "alt_par ",
            elif idec_opcode == HWY_SET_TIM:
                print "set_tim ",
                print " [",
                if idec_param & HWY_ST_RESET_TMR:
                    print "reset tim, ",
                if idec_param & 0x3 == HWY_ST_IOUPDATE:
                    print "pulse ioupdateo",
                elif idec_param & 0x3 == HWY_ST_PARALLEL:
                    print "pulse parallel",
                elif idec_param & 0x3 == HWY_ST_VGA:
                    print "pulse vga",
                elif idec_param & 0x3 == HWY_ST_3:
                    print "pulse 3",
                else:
                    pass
                print "]",
            elif idec_opcode == HWY_ALT_TIM:
                print "alt_tim ",
            elif idec_opcode == HWY_WAIT_FOR:
                print "wait_for ",
                op_wait_for = 1
            elif idec_opcode == HWY_SET_REG:
                print "set_reg ",
                op_set_reg = 1
            elif idec_opcode == HWY_BRANCH_NZ:
                print "brn_nz ",
                op_branch = 1
            elif idec_opcode == HWY_BRANCH:
                print "branch ",
                op_branch = 1
            else:
                print "UNKNOWN ",

            if op_wait_for:
                print " [",
                if idec_param & 0x4:
                    print "decrement reg %d, "%idec_param>>3,
                if idec_payload & 0x100:
                    print "VGA, ",
                if idec_payload & 0x40:
                    print "trigger, ",
                if (idec_payload & 0x1c0) == 0x1c0:
                    print " FINISH, ",
                print "] "
            elif op_branch:
                if idec_param & 0x4:
                    print "return",
                else:
                    print " 0x%03x (%3d) "%(idec_payload_10b, idec_payload_10b),
            elif op_set_reg:
                print " reg%d "%idec_param >> 3,
                print "0x%0xx (%4d)"%(idec_payload_12b, idec_payload_12b),
            else:
                print " 0x%03x (%3d)"%(idec_payload, idec_payload),

        
        if mdds_ser:
            ser_data = bram_data & 0xffff
            print "[15:0 0x%04x, ]"%ser_data,
            if bram_data>>16 == 0:
                print "freq lo \t%.2f kHz"%(ser_data*1000000.0/(0x100000000)),
            elif bram_data>>16 == 1:
                print "freq hi \t%.2f MHz"%(ser_data*1000.0/(0x10000)),
            elif bram_data>>16 == 2:
                print "phase \t%.2f deg (or CFR[15:0])"%(ser_data*360.0/0x10000),
            elif bram_data>>16 == 3:
                print "amp \t%.3f \t (or CFR[31:16])"%(ser_data*1.0/0x4000),

    print "\n"
                


def init(ddsm):
    # little VGA look up table

    m_write_lut =  True
    if  m_write_lut:
        lut = [0x0, 0x3ffe0]
        lut_addr = [0, 1023]
        vga_lut_s = VgaLutSubsetSetting(lut_addr, lut, ddsm, False, HWY_DDS_ALL_CHANNELS)
        ddsm.send(vga_lut_s)

    sync_tx_mask = 0x11111111 # first channel on each DDS card
    sync_rx_mask = 0xeeeeeeee # other three channels on each DDS card

    # DDS profile CFR Settings
    cfr1 = SpiCfrSetting(510, HWY_DDS_SINE_OUTPUT| HWY_AUTOCLR_PHASE_ACC, ddsm, False, HWY_DDS_ALL_CHANNELS)
    cfr2 = SpiCfrSetting(508, 0x014008a0, ddsm, False, HWY_DDS_ALL_CHANNELS)
    cfr3 = SpiCfrSetting(506, 0x1f3fc000, ddsm, False, HWY_DDS_ALL_CHANNELS)
    dds_sync_input_delay = 13

    # The sync settings share an address; a single sync Pulse will
    # write in two separate sets of Settings depending on the DDS
    # channel. Channel 1 on each board is the sync master, Channels
    # 2-4 are the sync slaves.
    cfr_sync_rx = SpiCfrSetting(504, 0x38000000|(dds_sync_input_delay << 18), ddsm, False, HWY_DDS_ALL_CHANNELS & sync_rx_mask) 
    cfr_sync_tx = SpiCfrSetting(504, 0x34000000|(dds_sync_input_delay << 18), ddsm, False, HWY_DDS_ALL_CHANNELS & sync_tx_mask)

    # CFR pulses
    cfr1_pulse = CfrPulse(0x00, cfr1, ddsm.m_cfr_time, ddsm, False)
    cfr2_pulse = CfrPulse(0x01, cfr2, ddsm.m_cfr_time, ddsm, False)
    cfr3_pulse = CfrPulse(0x02, cfr3, ddsm.m_cfr_time, ddsm, False)
    cfr_sync_pulse = CfrPulse(0x0a, cfr_sync_rx, ddsm.m_cfr_time, ddsm, False)

    # Init sequence
    iseq = InitSequence(ddsm, True, False, HWY_DDS_ALL_CHANNELS)
    iseq.append(cfr1_pulse)
    iseq.append(cfr2_pulse)
    iseq.append(cfr3_pulse)
    iseq.append(cfr_sync_pulse)

    # Transmit init sequence
    iseq.send_obj()

    # Transmit all the locally-defined Settings
    ddsm.send(cfr1)
    ddsm.send(cfr2)
    ddsm.send(cfr3)
    ddsm.send(cfr_sync_rx)
    ddsm.send(cfr_sync_tx)


    # Transmit all the globally-defined Settings stored in DdsManager
    ddsm.send(ddsm.m_cfr_time)
    ddsm.send(ddsm.m_zero_time)
    ddsm.send(ddsm.m_short_time)


            




