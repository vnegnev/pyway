#from hwy_hw import *
import hwy_hw
from hwy_selftest import *
from hwy_setting import *
from hwy_pulse import *
from hwy_sequence import *
from hwy_boss import *
from hwy_util import *

def boss_edge_selftest(ddsm, write_lut, reset_dds, starting_dds):
    # -------------- Freq, phase and amp settings
    # 3 parameters for 'carrier' word
    car_f = Freq(hwy_hw.freq_to_ftw(150), ddsm)
    car_p = Phase(hwy_hw.phase_to_ptw(0), ddsm)
    car_a = Amp(hwy_hw.amp_to_atw(100), ddsm)

    # 3 parameters for 'red sideband' word
    rsb_f = Freq(hwy_hw.freq_to_ftw(100), ddsm)
    rsb_p = Phase(hwy_hw.phase_to_ptw(90), ddsm)
    rsb_a = Amp(hwy_hw.amp_to_atw(75), ddsm)

    # 3 parameters for 'blue sideband' word
    bsb_f = Freq(hwy_hw.freq_to_ftw(80), ddsm)
    bsb_p = Phase(hwy_hw.phase_to_ptw(90), ddsm)
    bsb_a = Amp(hwy_hw.amp_to_atw(50), ddsm)

    # 'off' freq/amp/phase
    off_f = Freq(hwy_hw.freq_to_ftw(0), ddsm)
    off_p = Phase(hwy_hw.phase_to_ptw(0), ddsm)
    off_a = Amp(hwy_hw.amp_to_atw(0), ddsm)

    # ----------------Construct the Pulse

    # Rising edges for carrier, RSB, and BSB
    car_ri = Edge(car_f, car_p, car_a, off_t, 0, ddsm)
    rsb_ri = Edge(rsb_f, rsb_p, rsb_a, off_t, 0, ddsm)
    bsb_ri = Edge(bsb_f, bsb_p, bsb_a, off_t, 0, ddsm)

    # Falling edges for carrier single-qubit and carrier M-S gate
    car_sq_fa = Edge(off_f, off_p, off_a, on_sq_t, 0, ddsm)
    car_ms_fa = Edge(off_f, off_p, off_a, on_ms_t, 0, ddsm)

    # Falling edges for RSB single-qubit and carrier M-s gate
    rsb_sq_fa = Edge(off_f, off_p, off_a, on_sq_t, 0, ddsm)
    rsb_ms_fa = Edge(off_f, off_p, off_a, on_ms_t, 0, ddsm)

    # Falling edges for BSB single-qubit and carrier M-s gate
    bsb_sq_fa = Edge(off_f, off_p, off_a, on_sq_t, 0, ddsm)
    bsb_ms_fa = Edge(off_f, off_p, off_a, on_ms_t, 0, ddsm)

    # Start Pulse after a trigger
    start = Start(start_t, True, ddsm)

    # ------------------ Constructiong the sequences
    assert(start_dds <= HWY_DDS_CHAN-4)
    DDS1 = 1 << starting_dds
    DDS2 = 2 << starting_dds
    DDS3 = 4 << starting_dds
    DDS4 = 8 << starting_dds
    cooling_cycles = 10
    ddsm.append_all(start)

    ddsm.append(car_ri, DDS1)
    ddsm.append(car_sq_fa, DDS1)

    ddsm.append(rsb_ri, DDS2|DDS3)
    ddsm.append(rsb_sq_fa, DDS2|DDS3)

    ddsm.append(bsb_ri, DDS3|DDS4)
    ddsm.append(bsb_sq_fa, DDS3|DDS4)

    # Molmer-Sorensen pulse
    ddsm.append_ns(bsb_ri, DDS1)
    ddsm.append_ns(rsb_ri, DDS2)
    ddsm.sync()
    ddsm.append_ns(bsb_ms_fa, DDS1)
    ddsm.append_ns(rsb_ms_fa, DDS2)
    ddsm.sync()

    # Loop of pulses
    ddsm.start_subloop(cooling_cycles)

    ddsm.append(rsb_ri, DDS2|DDS3)
    ddsm.append(rsb_sq_fa, DDS2|DDS3)

    ddsm.append(bsb_ri, DDS1|DDS4)
    ddsm.append(bsb_sq_fa, DDS1|DDS4)

    ddsm.append_ns(bsb_ri, DDS2)
    ddsm.append_ns(rsb_ri, DDS3)
    ddsm.append_ns(car_ri, DDS4)
    ddsm.sync()
    
    ddsm.append_ns(bsb_ms_fa, DDS2) 
    ddsm.append_ns(rsb_ms_fa, DDS3)
    ddsm.append_ns(car_ms_fa, DDS4)
    ddsm.sync()

    # Specify which Settings will be swept inside the SubLoop
    # (note that any future uses of these Settings will take the )
    # values last pused to the FIFO)
    ddsm.dyn_update(car_a)
    ddsm.dyn_update(bsb_f)
    ddsm.dyn_update(rsb_p)

    ddsm.end_subloop()

    # ----------------- Reset DDSes, idecoders and minotaurs, and timeers
    if reset_dds:
        ddsm.halt_and_reset_dds()
    else:
        ddsm.halt_dds()

    # Initialise the CFRs, fixed time delays, etc
    init(ddsm)

    # Transmit the Sequences ( and Pulses) and Settings
    ddsm.send_all()

    # Transmit the Sequences (and Pulses) and Settings
    ddsm.send_all()

    ddsm.run_dds()

    # Send over the FIFO words
    for k in range(0, cooling_cysles-1):
        car_a.set_data(hwy_hw.amp_to_atw(15+5*k))
        bsb_f.set_data(hwy_hw.freq_to_ftw(20 + 10*k))
        rsb_p.set_data(hwy_hw.phase_to_ptw(45+45*k))

        ddsm.send_to_fifo(car_a)
        ddsm.send_to_fifo(bsb_f)
        ddsm.send_to_fifo(rsb_p)

    # Set values to the end_point values
    car_a.set_data(hwy_hw.amp_to_atw(100))
    bsb_f.set_data(hwy_hw.freq_to_ftw(80))
    rsb_p.set_data(hwy_hw.phase_to_ptw(90))

    ddsm.send_to_fifo(car_a)
    ddsm.send_to_fifo(bsb_f)
    ddsm.send_to_fifo(rsb_p)

    return 0

def boss_cap_selftest(ddsm, write_lut, reset_dds, starting_dds):
    # Settings
    freq_carr = Freq(hwy_hw.freq_to_ftw(125), ddsm)
    ph_carr = Phase(hwy_hw.phase_to_ptw(0), ddsm)
    amp_carr = Amp(hwy_hw.amp_to_atw(100), ddsm)
    t_carr_on = Time(hwy_hw.time_to_ticks(4), True, ddsm)
    t_start = Time(hwy_hw.time_to_ticks(2), True, ddsm)

    # Edges
    carr_ri = Edge(freq_carr, ph_carr, amp_carr, dds.m_profile_time, 0, ddsm)
    carr_fa = Edge(ddsm.m_off_freq, ddsm.m_off_ph, ddsm.m_off_amp, t_carr_on, 0, ddsm)

    # Caps
    carr = Cap(freq_carr, ph_carr, amp_carr, t_carr_on, 0, ddsm)

    # Start
    start = Start(t_start, True, ddsm)

    # --------------- Constructiong the sequences
    assert(starting_dds <= HWY_DDS_CHAN-4)
    DDS1 = 1 << starting_dds
    DDS2 = 2 << starting_dds
    DDS3 = 4 << starting_dds
    DDS4 = 8 << starting_dds
    ddsm.append_all(start)
    ddsm.append(carr, DDS1)
    ddsm.append(carr, DDS2)
    ddsm.append(carr, DDS3)
    ddsm.append(carr, DDS4)
    ddsm.append(carr, DDS1|DDS2)

    # ---------------- Reset DDSes, idecoders and minotaurs, and timers
    if reset_dds:
        ddsm.halt_and_reset_dds()
    else:
        ddsm.halt_dds()

    init(ddsm)
    ddsm.send_all()
    ddsm.run_dds()

    return 0 

def edge_sequence_selftest(ddsm, use_Sequence, write_lut, trig_loop, reset_dds):
    # Initialise DDS manager and global hiway base address

    DDS1_all_cards = 0x00001111
    DDS2_all_cards = 0x00002222
    DDS3_all_cards = 0x00004444
    DDS4_all_cards = 0x00008888

    # ----------------- Freq phase and amp settings

    # 3 parameters for 'on' word
    on_f = SpiFreqSetting(hwy_hw.freq_to_ftw(150), ddsm)
    on_p = SpiPhaseSetting(hwy_hw.phase_to_ptw(0), ddsm)
    on_a = SpiAmpSetting(hwy_hw.amp_to_atw(100), ddsm)

    # 3 parameters for 'off' word
    off_f = SpiFreqSetting(hwy_hw.freq_to_ftw(62.5), ddsm)
    off_p = SpiPhaseSetting(hwy_hw.phase_to_ptw(180), ddsm)
    off_a = SpiAmpSetting(hwy_hw.amp_to_atw(0), ddsm)

    # 3 parameters for 'mid' word
    mid_f = SpiFreqSetting(hwy_hw.freq_to_ftw(80), ddsm)
    mid_p = SpiPhaseSetting(hwy_hw.phase_to_ptw(90), ddsm)
    mid_a = SpiAmpSetting(hwy_hw.amp_to_atw(50), ddsm)

    # ------------------ Time Settings
    on_p_t = TimeSetting(hwy_hw.time_to_ticks(2), True, ddsm)
    off_p_t = TimeSetting(hwy_hw.time_to_ticks(5), True, ddsm)
    mid_p_t = TimeSetting(hwy_hw.time_to_ticks(3.5), True, ddsm)
    start_t = TimeSetting(hwy_hw.time_to_ticks(4)-14, True, ddsm) #offset of some number after a trigger
    wait_t = TimeSetting(hwy_hw.time_to_ticks(10), True, ddsm)

    # ------------------- Pulses

    # Create three edge Pulses for 'on',, 'off', and 'intermediate' states
    edge_on = EdgePulse(on_f, on_p, on_a, on_p_t, 0, ddsm)
    edge_off = EdgePulse(off_f, off_p, off_a, off_p_t, 0, ddsm)
    edge_mid = EdgePulse(mid_f, mid_p, mid_a, mid_p_t, 0, ddsm)

    # Create a delay pulse
    wait = WaitPulse(wait_t, ddsm)

    # Start pulse after a trigger
    start = StartPulse(start_t, trig_loop, ddsm)

    # Create and write a short Sequence
    lseq1 = LoopSequence(ddsm, True, DDS1_all_cards)
    lseq23 = LoopSequence(ddsm, True, DDS2_all_cards | DDS3_all_cards)
    lseq4 = LoopSequence(ddsm, True, DDS4_all_cards)

    # presence of a trigger determines whether start pulse should be in init or loop
    lseq1.append(start)
    lseq23.append(start)
    lseq4.append(start)

    lseq1.append(edge_on)
    lseq1.append(edge_off)
    lseq1.append(edge_mid)
    lseq1.append(edge_off)
    lseq1.append(edge_on)
    lseq1.append(wait)
    lseq1.append(edge_off)
    lseq1.append(wait)
    
    lseq23.append(wait)
    lseq23.append(edge_on)
    lseq23.append(edge_mid)
    lseq23.append(edge_on)
    lseq23.append(wait)
    lseq23.append(edge_off)

    lseq4.append(edge_on)
    lseq4.append(edge_mid)
    lseq4.append(edge_off)

    # Reset DDSes, idecoders and minotaurs, and timers
    if reset_dds:
        ddsm.halt_and_reset_dds()
    else:
        ddsm.halt_dds()

    # Initialise the CFRs, fixed time delays, etc
    init(ddsm)

    # Create a sub_loop sequence
    subloop = SubLoop(10, ddsm)

    # add subloop into loop
    lseq1.append(subloop)

    # Add two Pulses into subloop (Must be after adding subloop into loop, otherwise fubloop's channel mask is undefined) 
    subloop.append(edge_on)
    subloop.append(edge_off)

    # Specify to Subloop which Settings will be dynamically updated by FIFO
    subloop.dyn_update(on_f)
    subloop.dyn_update(on_a)

    # Transmit the Pulse sequences
    ddsm.send(lseq1)
    ddsm.send(lseq23)
    ddsm.send(lseq4)

    # Next, transmit all the Settings
    ddsm.send_settings()

    # Un-reset DDSes (run sequence)
    # (note: control word sets VGA reset amplitude to be high)
    ddsm.run_dds()

    # Send a bunch of FIFO settings so that they'll be popped by the inner loop
    for b in range(0,17):
        for z in range(1, 10):
            # sweep the amplitude
            on_a.set_data(hwy_hw.amp_to_atw(100-10*z))
            ddsm.send_to_fifo(on_a)

            # sweep the frequency; each freq result takes 2 fifo values
            on_f.set_data(hwy_hw.freq_to_ftw(150-10*z))
            ddsm.send_to_fifo(on_f)
        on_a.set_data(hwy_hw.amp_to_atw(100)) # restore defaults for start of next sweep
        ddsm.send_to_fifo(on_a)
        on_f.set_data(hwy_hw.freq_to_ftw(150))
        ddsm.send_to_fifo(on_f)

     # Clean-up is handled by DdsManager
    return 0 

      

default_vga_lut = [0x0, 0x1, 0x21, 0x41, 0x61, 0x82, 0xc2, 0x102, 0x143, 0x1a3, 0x203, 0x263,\
                             0x2c4, 0x344, 0x3c4, 0x444, 0x4c5, 0x565, 0x605, 0x6a6, 0x765, 0x806, 0x8c6,\
                             0x987, 0xa66, 0xb27, 0xc07, 0xce8, 0xde7, 0xec8, 0xfc8, 0x10c9, 0x11e8, 0x12e9,\
                             0x1409, 0x1529, 0x1649, 0x176a, 0x18aa, 0x19ea, 0x1b2a, 0x1c6a, 0x1dab, 0x1f0b,\
                             0x206b, 0x21cb, 0x232b, 0x248c, 0x260b, 0x276c, 0x28ec, 0x2a6d, 0x2c0c, 0x2d8c,\
                             0x2f0d, 0x30ad, 0x324d, 0x33ed, 0x358d, 0x372e, 0x38ed, 0x3a8e, 0x3c4e, 0x3e0e,\
                             0x3fce, 0x418e, 0x434e, 0x450f, 0x46ee, 0x48af, 0x4a8f, 0x4c6f, 0x4e4f, 0x502f,\
                             0x520f, 0x53f0, 0x55ef, 0x57cf, 0x59b0, 0x5bb0, 0x5daf, 0x5f90, 0x6190, 0x6390,\
                             0x6590, 0x6790, 0x6991, 0x6bb0, 0x6db0, 0x6fb1, 0x71d0, 0x73d1, 0x75f0, 0x77f1,\
                             0x7a11, 0x7c30, 0x7e31, 0x8051, 0x8271, 0x8491, 0x86b1, 0x88d1, 0x8af1, 0x8d11,\
                             0x8f31, 0x9151, 0x9372, 0x95b1, 0x97d1, 0x99f1, 0x9c12, 0x9e51, 0xa071, 0xa292,\
                             0xa4d1, 0xa6f1, 0xa912, 0xab51, 0xad72, 0xafb1, 0xb1d2, 0xb411, 0xb632, 0xb871,\
                             0xba92, 0xbcd1, 0xbef2, 0xc131, 0xc352, 0xc591, 0xc7b2, 0xc9f2, 0xcc31, 0xce52,\
                             0xd091, 0xd2b2, 0xd4f1, 0xd712, 0xd951, 0xdb72, 0xddb1, 0xdfd2, 0xe211, 0xe432,\
                             0xe671, 0xe892, 0xead1, 0xecf1, 0xef12, 0xf151, 0xf372, 0xf5b1, 0xf7d1, 0xf9f2,\
                             0xfc31, 0xfe51, 0x10072, 0x102b1, 0x104d1, 0x106f1, 0x10911, 0x10b32, 0x10d71,\
                             0x10f91, 0x111b1, 0x113d1, 0x115f1, 0x11811, 0x11a31, 0x11c51, 0x11e71,\
                             0x12091, 0x122b1, 0x124d1, 0x126f1, 0x12911, 0x12b30, 0x12d31, 0x12f51,\
                             0x13171, 0x13390, 0x13591, 0x137b1, 0x139d0, 0x13bd1, 0x13df1, 0x14010,\
                             0x14211, 0x14430, 0x14630, 0x14831, 0x14a50, 0x14c51, 0x14e70, 0x15070,\
                             0x15271, 0x15490, 0x15690, 0x15890, 0x15a90, 0x15c90, 0x15e90, 0x16090,\
                             0x16290, 0x16490, 0x16690, 0x16890, 0x16a90, 0x16c90, 0x16e90, 0x17090,\
                             0x1728f, 0x17470, 0x17670, 0x1786f, 0x17a50, 0x17c50, 0x17e4f, 0x18030,\
                             0x1822f, 0x18410, 0x1860f, 0x187f0, 0x189ef, 0x18bcf, 0x18db0, 0x18faf,\
                             0x1918f, 0x1936f, 0x1954f, 0x1972f, 0x19910, 0x19b0f, 0x19cef, 0x19ecf,\
                             0x1a0af, 0x1a28e, 0x1a44f, 0x1a62f, 0x1a80f, 0x1a9ef, 0x1abcf, 0x1adae,\
                             0x1af6f, 0x1b14f, 0x1b32e, 0x1b4ef, 0x1b6ce, 0x1b88f, 0x1ba6e, 0x1bc2f,\
                             0x1be0e, 0x1bfcf, 0x1c1ae, 0x1c36e, 0x1c52f, 0x1c70e, 0x1c8ce, 0x1ca8e,\
                             0x1cc4e, 0x1ce0f, 0x1cfee, 0x1d1ae, 0x1d36e, 0x1d52e, 0x1d6ee, 0x1d8ae,\
                             0x1da6e, 0x1dc2d, 0x1ddce, 0x1df8e, 0x1e14e, 0x1e30e, 0x1e4cd, 0x1e66e,\
                             0x1e82e, 0x1e9ed, 0x1eb8e, 0x1ed4d, 0x1eeee, 0x1f0ad, 0x1f24e, 0x1f40d,\
                             0x1f5ad, 0x1f74e, 0x1f90d, 0x1faad, 0x1fc4e, 0x1fe0d, 0x1ffad, 0x2014d,\
                             0x202ed, 0x2048d, 0x2062d, 0x207cd, 0x2096d, 0x20b0d, 0x20cad, 0x20e4d,\
                             0x20fed, 0x2118d, 0x2132d, 0x214cd, 0x2166c, 0x217ed, 0x2198d, 0x21b2c,\
                             0x21cad, 0x21e4d, 0x21fec, 0x2216d, 0x2230c, 0x2248d, 0x2262c, 0x227ad,\
                             0x2294c, 0x22acc, 0x22c4d, 0x22dec, 0x22f6c, 0x230ec, 0x2326d, 0x2340c,\
                             0x2358c, 0x2370c, 0x2388c, 0x23a0c, 0x23b8c, 0x23d0c, 0x23e8c, 0x2400c,\
                             0x2418c, 0x2430c, 0x2448c, 0x2460c, 0x2478c, 0x2490b, 0x24a6c, 0x24bec,\
                             0x24d6c, 0x24eeb, 0x2504c, 0x251cb, 0x2532c, 0x254ac, 0x2562b, 0x2578c,\
                            0x2590b, 0x25a6c, 0x25beb, 0x25d4b, 0x25eac, 0x2602b, 0x2618b, 0x262ec,\
                            0x2646b, 0x265cb, 0x2672b, 0x2688b, 0x269ec, 0x26b6b, 0x26ccb, 0x26e2b,\
                            0x26f8b, 0x270eb, 0x2724b, 0x273ab, 0x2750b, 0x2766b, 0x277ca, 0x2790b,\
                            0x27a6b, 0x27bcb, 0x27d2b, 0x27e8a, 0x27fcb, 0x2812b, 0x2828b, 0x283ea,\
                            0x2852b, 0x2868a, 0x287cb, 0x2892a, 0x28a6b, 0x28bca, 0x28d0b, 0x28e6a,\
                            0x28fab, 0x2910a, 0x2924a, 0x2938b, 0x294ea, 0x2962a, 0x2976b, 0x298ca,\
                            0x29a0a, 0x29b4a, 0x29c8a, 0x29dcb, 0x29f2a, 0x2a06a, 0x2a1aa, 0x2a2ea,\
                            0x2a42a, 0x2a56a, 0x2a6aa, 0x2a7ea, 0x2a92a, 0x2aa6a, 0x2aba9, 0x2acca,\
                            0x2ae0a, 0x2af4a, 0x2b08a, 0x2b1c9, 0x2b2ea, 0x2b42a, 0x2b56a, 0x2b6a9,\
                            0x2b7ca, 0x2b909, 0x2ba2a, 0x2bb6a, 0x2bca9, 0x2bdca, 0x2bf09, 0x2c02a,\
                            0x2c169, 0x2c289, 0x2c3aa, 0x2c4e9, 0x2c60a, 0x2c749, 0x2c869, 0x2c989,\
                            0x2caaa, 0x2cbe9, 0x2cd09, 0x2ce29, 0x2cf4a, 0x2d089, 0x2d1a9, 0x2d2c9,\
                            0x2d3e9, 0x2d509, 0x2d629, 0x2d749, 0x2d869, 0x2d989, 0x2daa9, 0x2dbc9,\
                            0x2dce9, 0x2de09, 0x2df29, 0x2e048, 0x2e149, 0x2e269, 0x2e389, 0x2e4a9,\
                            0x2e5c8, 0x2e6c9, 0x2e7e9, 0x2e908, 0x2ea09, 0x2eb29, 0x2ec48, 0x2ed49,\
                            0x2ee68, 0x2ef69, 0x2f088, 0x2f189, 0x2f2a8, 0x2f3a9, 0x2f4c8, 0x2f5c9,\
                            0x2f6e8, 0x2f7e8, 0x2f8e9, 0x2fa08, 0x2fb08, 0x2fc09, 0x2fd28, 0x2fe28,\
                            0x2ff28, 0x30029, 0x30148, 0x30248, 0x30348, 0x30448, 0x30548, 0x30648,\
                            0x30748, 0x30849, 0x30968, 0x30a68, 0x30b68, 0x30c67, 0x30d48, 0x30e48,\
                            0x30f48, 0x31048, 0x31148, 0x31248, 0x31348, 0x31447, 0x31528, 0x31628,\
                            0x31728, 0x31827, 0x31908, 0x31a08, 0x31b07, 0x31be8, 0x31ce8, 0x31de7,\
                            0x31ec8, 0x31fc7, 0x320a8, 0x321a8, 0x322a7, 0x32388, 0x32487, 0x32567,\
                            0x32648, 0x32747, 0x32828, 0x32927, 0x32a07, 0x32ae8, 0x32be7, 0x32cc7,\
                            0x32da8, 0x32ea7, 0x32f87, 0x33067, 0x33148, 0x33247, 0x33327, 0x33407,\
                            0x334e7, 0x335c7, 0x336a7, 0x33787, 0x33867, 0x33948, 0x33a47, 0x33b27,\
                            0x33c07, 0x33ce6, 0x33da7, 0x33e87, 0x33f67, 0x34047, 0x34127, 0x34207,\
                            0x342e7, 0x343c6, 0x34487, 0x34567, 0x34647, 0x34727, 0x34806, 0x348c7,\
                            0x349a7, 0x34a86, 0x34b47, 0x34c27, 0x34d06, 0x34dc7, 0x34ea6, 0x34f67,\
                            0x35047, 0x35126, 0x351e7, 0x352c6, 0x35387, 0x35466, 0x35526, 0x355e7,\
                            0x356c6, 0x35787, 0x35866, 0x35926, 0x359e7, 0x35ac6, 0x35b86, 0x35c47,\
                            0x35d26, 0x35de6, 0x35ea6, 0x35f66, 0x36027, 0x36106, 0x361c6, 0x36286,\
                            0x36346, 0x36406, 0x364c6, 0x36587, 0x36666, 0x36726, 0x367e6, 0x368a6,\
                            0x36966, 0x36a26, 0x36ae6, 0x36ba6, 0x36c65, 0x36d06, 0x36dc6, 0x36e86,\
                            0x36f46, 0x37006, 0x370c6, 0x37185, 0x37226, 0x372e6, 0x373a6, 0x37465,\
                            0x37506, 0x375c6, 0x37686, 0x37745, 0x377e6, 0x378a6, 0x37965, 0x37a06,\
                            0x37ac5, 0x37b66, 0x37c25, 0x37cc6, 0x37d86, 0x37e45, 0x37ee6, 0x37fa5,\
                            0x38045, 0x380e6, 0x381a5, 0x38246, 0x38305, 0x383a6, 0x38465, 0x38505,\
                            0x385a6, 0x38665, 0x38705, 0x387a5, 0x38846, 0x38905, 0x389a5, 0x38a45,\
                            0x38ae6, 0x38ba5, 0x38c45, 0x38ce5, 0x38d85, 0x38e25, 0x38ec5, 0x38f66,\
                            0x39025, 0x390c5, 0x39165, 0x39205, 0x392a5, 0x39345, 0x393e5, 0x39485,\
                            0x39525, 0x395c5, 0x39664, 0x396e5, 0x39785, 0x39825, 0x398c5, 0x39965,\
                            0x39a05, 0x39aa4, 0x39b25, 0x39bc5, 0x39c65, 0x39d04, 0x39d85, 0x39e25,\
                            0x39ec5, 0x39f64, 0x39fe5, 0x3a085, 0x3a124, 0x3a1a5, 0x3a244, 0x3a2c5,\
                            0x3a365, 0x3a404, 0x3a485, 0x3a524, 0x3a5a5, 0x3a644, 0x3a6c5, 0x3a764,\
                            0x3a7e5, 0x3a884, 0x3a904, 0x3a985, 0x3aa24, 0x3aaa5, 0x3ab44, 0x3abc4,\
                            0x3ac45, 0x3ace4, 0x3ad64, 0x3ade5, 0x3ae84, 0x3af04, 0x3af84, 0x3b005,\
                            0x3b0a4, 0x3b124, 0x3b1a4, 0x3b224, 0x3b2a4, 0x3b325, 0x3b3c4, 0x3b444,\
                            0x3b4c4, 0x3b544, 0x3b5c4, 0x3b644, 0x3b6c4, 0x3b744, 0x3b7c4, 0x3b844,\
                            0x3b8c4, 0x3b944, 0x3b9c4, 0x3ba44, 0x3bac4, 0x3bb44, 0x3bbc4, 0x3bc43,\
                            0x3bca4, 0x3bd24, 0x3bda4, 0x3be24, 0x3bea4, 0x3bf23, 0x3bf84, 0x3c004,\
                            0x3c084, 0x3c103, 0x3c164, 0x3c1e4, 0x3c263, 0x3c2c4, 0x3c344, 0x3c3c3,\
                            0x3c424, 0x3c4a4, 0x3c523, 0x3c584, 0x3c603, 0x3c664, 0x3c6e3, 0x3c744,\
                            0x3c7c3, 0x3c824, 0x3c8a3, 0x3c904, 0x3c983, 0x3c9e4, 0x3ca63, 0x3cac4,\
                            0x3cb43, 0x3cba3, 0x3cc04, 0x3cc83, 0x3cce3, 0x3cd44, 0x3cdc3, 0x3ce23,\
                            0x3ce84, 0x3cf03, 0x3cf63, 0x3cfc3, 0x3d024, 0x3d0a3, 0x3d103, 0x3d163,\
                            0x3d1c3, 0x3d223, 0x3d284, 0x3d303, 0x3d363, 0x3d3c3, 0x3d423, 0x3d483,\
                            0x3d4e3, 0x3d543, 0x3d5a3, 0x3d603, 0x3d663, 0x3d6c3, 0x3d723, 0x3d783,\
                            0x3d7e3, 0x3d843, 0x3d8a3, 0x3d903, 0x3d963, 0x3d9c2, 0x3da03, 0x3da63,\
                            0x3dac3, 0x3db23, 0x3db83, 0x3dbe2, 0x3dc23, 0x3dc83, 0x3dce3, 0x3dd42,\
                            0x3dd83, 0x3dde3, 0x3de42, 0x3de83, 0x3dee3, 0x3df42, 0x3df83, 0x3dfe3,\
                            0x3e042, 0x3e083, 0x3e0e2, 0x3e123, 0x3e183, 0x3e1e2, 0x3e223, 0x3e282,\
                            0x3e2c3, 0x3e322, 0x3e363, 0x3e3c2, 0x3e403, 0x3e462, 0x3e4a2, 0x3e4e3,\
                            0x3e542, 0x3e582, 0x3e5c3, 0x3e622, 0x3e663, 0x3e6c2, 0x3e702, 0x3e742,\
                           0x3e783, 0x3e7e2, 0x3e822, 0x3e862, 0x3e8a3, 0x3e902, 0x3e942, 0x3e982,\
                            0x3e9c2, 0x3ea03, 0x3ea62, 0x3eaa2, 0x3eae2, 0x3eb22, 0x3eb62, 0x3eba2,\
                            0x3ebe2, 0x3ec22, 0x3ec62, 0x3eca2, 0x3ece2, 0x3ed22, 0x3ed62, 0x3eda2,\
                            0x3ede2, 0x3ee22, 0x3ee62, 0x3eea2, 0x3eee2, 0x3ef22, 0x3ef62, 0x3efa2,\
                            0x3efe1, 0x3f002, 0x3f042, 0x3f082, 0x3f0c2, 0x3f101, 0x3f122, 0x3f162,\
                            0x3f1a2, 0x3f1e1, 0x3f202, 0x3f242, 0x3f281, 0x3f2a2, 0x3f2e2, 0x3f321,\
                            0x3f342, 0x3f382, 0x3f3c1, 0x3f3e2, 0x3f421, 0x3f442, 0x3f481, 0x3f4a2,\
                            0x3f4e2, 0x3f521, 0x3f542, 0x3f581, 0x3f5a2, 0x3f5e1, 0x3f601, 0x3f622,\
                            0x3f661, 0x3f682, 0x3f6c1, 0x3f6e1, 0x3f702, 0x3f741, 0x3f761, 0x3f782,\
                            0x3f7c1, 0x3f7e1, 0x3f802, 0x3f841, 0x3f861, 0x3f881, 0x3f8a2, 0x3f8e1,\
                            0x3f901, 0x3f921, 0x3f941, 0x3f962, 0x3f9a1, 0x3f9c1, 0x3f9e1, 0x3fa01,\
                            0x3fa21, 0x3fa41, 0x3fa61, 0x3fa81, 0x3faa1, 0x3fac1, 0x3fae2, 0x3fb21,\
                            0x3fb41, 0x3fb60, 0x3fb61, 0x3fb81, 0x3fba1, 0x3fbc1, 0x3fbe1, 0x3fc01,\
                            0x3fc21, 0x3fc41, 0x3fc61, 0x3fc81, 0x3fca0, 0x3fca1, 0x3fcc1, 0x3fce1,\
                            0x3fd01, 0x3fd20, 0x3fd21, 0x3fd41, 0x3fd61, 0x3fd80, 0x3fd81, 0x3fda1,\
                            0x3fdc0, 0x3fdc1, 0x3fde1, 0x3fe00, 0x3fe01, 0x3fe21, 0x3fe40, 0x3fe41,\
                            0x3fe60, 0x3fe61, 0x3fe80, 0x3fe81, 0x3fea0, 0x3fea1, 0x3fec0, 0x3fec1,\
                            0x3fee0, 0x3fee1, 0x3ff00, 0x3ff01, 0x3ff20, 0x3ff20, 0x3ff21, 0x3ff40,\
                            0x3ff41, 0x3ff60, 0x3ff60, 0x3ff61, 0x3ff80, 0x3ff80, 0x3ff80, 0x3ff81,\
                            0x3ffa0, 0x3ffa0, 0x3ffa0, 0x3ffa1, 0x3ffc0, 0x3ffc0, 0x3ffc0, 0x3ffc0,\
                            0x3ffc1, 0x3ffe0, 0x3ffe0, 0x3ffe0, 0x3ffe0, 0x3ffe0, 0x3ffe0, 0x3ffe0,\
           0x3ffe0, 0x3ffe0, 0x3ffe0, 0x3ffe0, 0x3ffe0]
